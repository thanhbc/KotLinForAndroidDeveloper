# Standard Delegates

There exists a set of standard delegates included in the Kotlin standard library. These are the most common situations where a delegate is really useful, but we could also create our own.

### Lazy
It takes a lambda that is executed the first time `getValue` is called, so the initialisation of the property is delayed up to that moment. Subsequent calls will return the same value. This is very interesting for things that are not always necessary and/or require some other parts to be ready before this one is used. We can save memory and skip the initialisation until the property is required.

```kotlin
class App : Application() {
    val database: SQLiteOpenHelper by lazy {
        MyDatabaseHelper(applicationContext)
    }

    override fun onCreate() {
        super.onCreate()
        val db = database.writableDatabase
    }
}
```

In this example, the database is not really initialised until it’s called first time in onCreate. At that moment, we are sure the application context exists and is ready to be used. The `lazy` operation is thread safe.

You can also use `lazy(LazyThreadSafetyMode.NONE) { ... }` if you’re not worried about multithread and want to get some extra performance.


### Observable

This delegate will help us detect changes on any property we need to observe. It will execute the lambda expression we specify, every time the `set` function is called. So after the new value is assigned, we receive the delegated property, the old value and the new one.
```kotlin
class ViewModel(val db: MyDatabase) {
    var myProperty by Delegates.observable("") {
        d, old, new ->
        db.saveChanges(this, new)
    }
}
```

This example represents some kind of `ViewModel` class which is aware of `myProperty` changes, and saves them to the database every time a new value is assigned


### Vetoable
This is a special kind of `observable` that lets you decide whether the value must be saved or not. It can be used to check some conditions before saving a value.

```kotlin
var positiveNumber = Delegates.vetoable(0) {
    d, old, new ->
    new >= 0
}
```

The previous delegate will only allow the new value to be saved if it’s a positive number. Inside lambdas, the latest line represents the return value. You don’t need to use the `return` word (it won’t compile indeed).


### lateinit

Sometimes we need something else to initialise a property, but we don’t have the required state available in the constructor, or we even are not able to access to them. This second case happens now and then in Android: in activities, fragments, services, broadcast receivers… However, a non abstract property needs a value before the constructor finishes executing. We cannot just wait until we want, in order to assign a value to the property. We have at least a couple of options.


The first one is to use a nullable type and set it to null, until we have the real value. But we then need to check everywhere throughout the code whether the property is null or not. If we are sure this property is not going to be null at any moment before using it, this may make us write some unnecessary code.

The second option is to use **lateinit**, which identifies that the property should have a non-nullable value, but its assignment will be delayed. If the value is requested before it is assigned, it will throw an exception that clearly identifies the property being accessed.

**lateinit** is not exactly a delegate, but a property modifier, and that’s why it must be written before the property.

This could be helpful in the App singleton example:

```kotlin
class App : Application() {
    companion object {
        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}
```

**lateinit** is also indispensable when working with a dependency injector, and very useful for tests too.


### Values from a map

Another way to delegate the values of a property is to get them from a map, using the name of the property as the key of the map. This delegate let us do really powerful things, because we can easily create an instance of an object from a dynamic map. If we are using immutable properties, the map can be immutable too. For mutable properties, the class will require a `MutableMap` as constructor parameter instead.

Imagine a configuration class we load from a Json, and assign those key and values to a map. We could just create an instance of a class by passing this map to the constructor:

```kotlin
class Configuration(map: Map<String, Any?>) {
    val width: Int by map
    val height: Int by map
    val dp: Int by map
    val deviceName: String by map
}
```

As a reference, here it is how we could create the necessary map for this class:

```kotlin
conf = Configuration(mapOf(
    "width" to 1080,
    "height" to 720,
    "dp" to 240,
    "deviceName" to "mydevice"
))
```
