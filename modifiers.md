# Modifiers


### private
The `private` modifier is the most restrictive we can use. It implies it will only be visible inside its own file. So if we declare a class as private, we won’t be able to use it outside the file where it was defined.

On the other hand, if we use `private` inside a class, the access is restricted to that class. Even classes that extend it won’t be able to use it.

So first level classes, objects, interfaces… (known as package members) declared as `private` are only visible inside the file where they are declared, while everything defined inside a class or interface will only be visible by that class or interface.

### protected

This modifier only applies to members inside a class or an interface. A package member cannot be `protected`. Inside a member, it works the same way as in Java: it can be used by the member itself and the members that extend it (for instance, a class and its subclasses).

### internal

An internal member is visible inside the whole module if it’s a package member. If it’s a member inside another scope, it depends on the visibility of the scope. For instance, if we write a private class, the access to an `internal` function will be limited to the visibility of the class.

We can use internal classes from any other class in the same module, but not from another module.

>What is a module?
>>According to Jetbrains definition, a module is a discrete unit of functionality which you can compile, run, test and debug independently. It basically refers to the Android Studio modules we can create to divide our project into different blocks. In Eclipse, these modules would refer to the projects inside a workspace.


### public
As you may guess, this is the less restrictive modifier. It’s the **default modifier**, and the member declared as public is visible anywhere, obviously restricted by its scope. A public member defined in a private class won’t be visible outside the scope where the class is visible.






