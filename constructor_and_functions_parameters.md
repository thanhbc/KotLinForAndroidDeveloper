# Constructor and functions parameters

Parameters trong Kotlin có một chút khác biệt với Java. Như bạn có thể thấy, chúng ta viết tên của param trước sau đó mới đến kiểu dữ liệu.
```kotlin
fun add(x: Int, y: Int) : Int {
    return x + y
}
```

Một điểm cực kỳ hữu ích về các param là chúng ta có thể tuỳ chọn bằng cách gắn cho chúng **giá trị mặc định**. Đây là ví dụ về một hàm mà bạn có thể dùng trong một activity, được sử dụng để toast một message:
```kotlin
fun toast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}
```

Như bạn có thể thấy, param thứ 2 (`length`) được xác định giá trị mặc định. Điều này nghĩa là bạn có thể truyền giá trị thứ 2 hoặc không, điều này sẽ tránh được việc phải viết function overloading:
```kotlin
toast("Hello")
toast("Hello", Toast.LENGTH_LONG)
```

Tương tự như đoạn code bên dưới trong Java:
```java
void toast(String message){
}

void toast(String message, int length){
    Toast.makeText(this, message, length).show();
}
```
Và nó cũng có thể phức tạp nếu muốn. Xem ví dụ này:
```kotlin
fun niceToast(message: String,
                tag: String = javaClass<MainActivity>().getSimpleName(),
                length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, "[$className] $message", length).show()
}
```

Tôi thêm vào param thứ 3 là tag với giá trị mặc định là tên class. Chúng ta sẽ cần viết thêm các phương thức overload theo cấp số nhân trong Java. Bây giờ bạn có thể gọi:
```kotlin
toast("Hello")
toast("Hello", "MyTag")
toast("Hello", "MyTag", Toast.LENGTH_SHORT)
```
Và có một điểm nữa, bởi vì các argument (đối số) có thể được sử dụng, nghĩa là bạn có thể viết tên của đối số trước rồi nhập giá trị xác định bạn muốn:
```kotlin
toast(message = "Hello", length = Toast.LENGTH_SHORT)
```
>Tip: String templates
>>Bạn có thể sử dụng template expressions trực tiếp trong chuỗi. Điều này sẽ dễ dàng hơn cho việc viết một chuỗi string phức tạp dựa trên các static và variable. Ở ví dụ trước, tôi đã sử dụng `"[$className] $message"`.
>>
>>Như bạn có thể thấy, bất kỳ khi nào bạn muốn thêm một expression, bạn cần sử dụng dấu `$`. Nếu expression trở nên phức tạp, bạn sẽ cần một vài dấu ngoặc `{}` `"Your name is ${user.name}"`.

