# Application Singleton

The simplest way is to just create a singleton the way we’d do in Java:
```kotlin
class App : Application() {
    companion object {
        private var instance: Application? = null
        fun instance() = instance!!
    }
    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}
```

Remember you need to add this `App` class to the `AndroidManifest.xml` in order to be used as the application instance:
```kotlin
<application
    android:allowBackup="true"
    android:icon="@mipmap/ic_launcher"
    android:label="@string/app_name"
    android:theme="@style/AppTheme"
    android:name=".ui.App">
    ...
</application>
```

The problem with Android is that we don’t have control over many class constructors. For instance, we cannot initialise a non-nullable property, because its value needs to be defined in the constructor. So we need a nullable variable and then a function that returns a non-nullable value. We know we always have an `App` instance, and that nothing under our control can be done before application `onCreate`, so we are safe by assuming `instance()` function will always be able to return a nonnullable `App` instance.

But this solution seems a bit unnatural. We need to define a property (which already has a getter and a setter) and then a function to return that property. Do we have another way to get a similar result? Yeah, we can delegate the value of a property to another class. This is commonly know as **delegated properties**.