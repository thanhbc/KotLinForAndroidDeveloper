# Variance

This is really one of the trickiest parts to understand. In Java, there is a problem when we use generic types. Logic says that `List<String>` should be able to be casted to `List<Object>` because it’s less restrictive. But take a look at this example:
```kotlin
List<String> strList = new ArrayList<>();
List<Object> objList = strList;
objList.add(5);
String str = objList.get(0);
```

If the Java compiler let us do this, we could add an `Integer` to an `Object` list, and this would obviously crash at some point. That’s why wildcards were added to the language. Wildcards will increase flexibility while limiting this problem.

If we add ‘`? extends Object`’ we are using **covariance**, which means that we can deal with any object that uses a type that is more restrictive than `Object`, but we can only do get operations safely. If we want to copy a collection of `Strings` into a collection of `Objects`, we should be allowed, right? Then, if we have:

```kotlin
List<String> strList = ...;
List<Object> objList = ...;
objList.addAll(strList);
```

This is possible because the definition of addAll() in Collection interface is something like:

```kotlin
List<String>
interface Collection<E> ... {
    void addAll(Collection<? extends E> items);
}
```

Otherwise, without the wildcard, we wouldn’t be allowed to use a `String` list with this method. The opposite, of course, would fail. We can’t use `addAll()` to add a list of Objects to a list of Strings. As we are only getting the items from the collection we use in that method, it’s a perfect example of covariance.

On the other hand, we can find **contravariance**, which is just the opposite situation. Following with
the `Collection` example, if we want to add items to a `Collection` we are passing as parameter, we could add objects with a more restrictive type into a more generic collection. For instance, we could add Strings to an `Object` list:
```kotlin
void copyStrings(Collection<? super String> to, Collection<String> from) {
    to.addAll(from);
}
```

The only restriction we have to add Strings to another collection is that the collection accepts `Objects` that are `Strings` or parent classes.

But wildcards have their own limitations. Wildcards define **use-site variance**, which means we need to declare it where we use it. This implies adding boilerplate every time we declare a more generic variable.

Let’s see an example. Using a class similar to the one we had before:

```kotlin
class TypedClass<T> {
    public T doSomething(){
        ...
    }
}
```

This code will not compile:
```kotlin
TypedClass<String> t1 = new TypedClass<>();
TypedClass<Object> t2 = t1;
```

Though it really doesn’t make sense, because we could still keep calling all the methods of the class, and nothing would break. We need to specify that the type can have a more flexible definition.
```kotlin
TypedClass<String> t1 = new TypedClass<>();
TypedClass<? extends String> t2 = t1;
```

This makes things more difficult to understand, and adds some extra boilerplate.

On the other hand, Kotlin deals with it in an easier manner by using **declaration-site** variance. This means that we specify that we can deal with less restrictive situations when defining the class or interface, and then we can use it blindly everywhere.

So let’s see how it looks in Kotlin. Instead of long wildcards, Kotlin just uses **out** for covariance and **in** for contravariance. In this case, as our class is producing objects that could be saved into less restrictive variables, we’ll be using covariance. We can define this in the class declaration directly:
```kotlin
class TypedClass<out T>() {
    fun doSomething(): T {
        ...
    }
}
```

And that’s all we need. Now, the same example that wouldn’t compile in Java is perfectly possible in Kotlin:

```kotlin
val t1 = TypedClass<String>()
val t2: TypedClass<Any> = t1
```

If you were already used to these concepts, I’m sure you will easily be able to use **in** and **out** in Kotlin. Otherwise, it just requires a little practice and some concepts understanding.