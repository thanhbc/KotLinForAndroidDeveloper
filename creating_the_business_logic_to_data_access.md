# Creating the business logic to data access
After implementing the access to the server and a way to interact with the database, it’s time to put things together. The logical steps would be:
1. Request the required data from the database
2. Check if there is data for the corresponding week
3. If the required data is found, it is returned to the UI to be rendered
4. Otherwise, the data is requested to the server
5. The result is saved in the database and returned to the UI to be rendered

But our commands shouldn’t need to deal with all this logic. The source of the data is an implementation detail that could easily be changed, so adding some extra code that abstracts the commands from the access to the data sounds like a good idea. In our implementation, it will iterate over a list of sources until a proper result is found.

So let’s start by specifying the interface any data source that wants to be used by our provider should implement:
```kotlin
interface ForecastDataSource {
    fun requestForecastByZipCode(zipCode: Long, date: Long): ForecastList?
}
```

The provider will require a function that receives a zip code and a date, and it should return a weekly forecast from that day.

```kotlin
class ForecastProvider(val sources: List<ForecastDataSource> =
        ForecastProvider.SOURCES) {

    companion object {
        val DAY_IN_MILLIS = 1000 * 60 * 60 * 24
        val SOURCES = listOf(ForecastDb(), ForecastServer())
    }
    ...
}
```

The forecast provider receives a list of sources, that once again can be specified through the constructor (for test purposes for instance), but I’m defaulting it to a `SOURCES`list defined in the companion object. It will use a database source and a server source. The order is important, because it will iterate over the sources, and the search will be stopped when any of the sources returns a valid result. The logical order is to search first locally (in the database) and then through the API.

So the main method looks like this:
```kotlin
fun requestByZipCode(zipCode: Long, days: Int): ForecastList
            = sources.firstResult { requestSource(it, days, zipCode) }
```

It will get the first result that is not null. When searching through the list of functional operators explained in chapter 18, I couldn’t find one that did exactly what I was looking for. So, as we have access to Kotlin sources, I just copied `first` function and modified it to behave as expected:
```kotlin
inline fun <T, R : Any> Iterable<T>.firstResult(predicate: (T) -> R?) : R {
    for (element in this){
        val result = predicate(element)
        if (result != null) return result
    }
    throw NoSuchElementException("No element matching predicate was found.")
}
```

The function receives a predicate which gets an object of type T and returns a value of type R?. This means that the predicate can return null, but our `firstResult` function can’t. That’s the reason why it returns a value of type R.

How it works? It will iterate and execute the predicate over the elements in the `Iterable` collection. When the result of the predicate is not null, this result will be returned.

If we wanted to include the case where all the sources can return null, we could have derived from `firstOrNull` function instead. The difference would consist of returning `null` instead of throwing an exception in the last line. But I’m not dealing with those details in this code.

In our example `T = ForecastDataSource` and `R = ForecastList`. But remember the function specified in `ForecastDataSource` returned a `ForecastList?`, which equals `R?`, so everything matches perfectly. The function `requestSource` just makes the previous function look more readable:

```kotlin
fun requestSource(source: ForecastDataSource, days: Int, zipCode: Long):
        ForecastList? {
    val res = source.requestForecastByZipCode(zipCode, todayTimeSpan())
    return if (res != null && res.size() >= days) res else null
}
```

The request is executed and only returns a value if the result is not null and the number of days matches the parameter. Otherwise, the source doesn’t have enough up-to-date data to return a successful result.

The function `todayTimeSpan()` calculates the time in milliseconds for the current day, eliminating the “time” offset, and keeping only the day. Some of the sources (in our case the database) may need it. The server defaults to today if we don’t send more information, so it won’t be used there.
```kotlin
private fun todayTimeSpan() = System.currentTimeMillis() / DAY_IN_MILLIS * DAY_IN_MILLIS
```
The complete code of this class would be:

```kotlin
class ForecastProvider(val sources: List<ForecastDataSource> =
        ForecastProvider.SOURCES) {

    companion object {
        val DAY_IN_MILLIS = 1000 * 60 * 60 * 24;
        val SOURCES = listOf(ForecastDb(), ForecastServer())
    }

    fun requestByZipCode(zipCode: Long, days: Int): ForecastList
            = sources.firstResult { requestSource(it, days, zipCode) }

    private fun requestSource(source: RepositorySource, days: Int,
            zipCode: Long): ForecastList? {
        val res = source.requestForecastByZipCode(zipCode, todayTimeSpan())
        return if (res != null && res.size() >= days) res else null
    }

    private fun todayTimeSpan() = System.currentTimeMillis() /
            DAY_IN_MILLIS * DAY_IN_MILLIS
}
```

We already defined `ForecastDb`. It just now needs to implement `ForecastDataSource`:

```kotlin
class ForecastDb(val forecastDbHelper: ForecastDbHelper =
        ForecastDbHelper.instance, val dataMapper: DbDataMapper = DbDataMapper())
        : ForecastDataSource {

    override fun requestForecastByZipCode(zipCode: Long, date: Long) =
            forecastDbHelper.use {
            ...
    }
    ...
}
```
The `ForecastServer` is not implemented yet, but it’s really simple. It will make use of a `ForecastDb` to save the response once it’s received from the server. That way, we can keep it cached into the database for future requests.
```kotlin
class ForecastServer(val dataMapper: ServerDataMapper = ServerDataMapper(),
        val forecastDb: ForecastDb = ForecastDb()) : ForecastDataSource {

    override fun requestForecastByZipCode(zipCode: Long, date: Long):
            ForecastList? {
        val result = ForecastByZipCodeRequest(zipCode).execute()
        val converted = dataMapper.convertToDomain(zipCode, result)
        forecastDb.saveForecast(converted)
        return forecastDb.requestForecastByZipCode(zipCode, date)
    }
}
```
It also makes use of a data mapper, the first one we created, though I modified the name of some methods to make it similar to the data mapper we used for the database model. You can take a look at the provider to see the details.

The overridden function makes the request to the server, converts the result to domain objects and saves them into the database. It finally returns the values from the database, because we need the row ids auto-generated by the insert query.

With these last steps, the provider is already implemented. Now we need to start using it. The `ForecastCommand` no longer should interact directly with server requests, nor convert the data to the domain model.
```kotlin
RequestForecastCommand(val zipCode: Long,
        val forecastProvider: ForecastProvider = ForecastProvider()) :
        Command<ForecastList> {

    companion object {
        val DAYS = 7
    }

    override fun execute(): ForecastList {
        return forecastProvider.requestByZipCode(zipCode, DAYS)
    }
}
```
The rest of code modifications consist of some renames and package organisation here and there. Take a look at the corresponding commit at [Kotlin for Android Developers repository²⁴].
[Kotlin for Android Developers repository²⁴]:https://github.com/antoniolg/Kotlin-for-Android-Developers