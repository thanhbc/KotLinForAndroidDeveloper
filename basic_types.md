# Basic types

Tất nhiên, các kiểu dữ liệu cơ bản như integers, floats, characters hoặc booleans vẫn tồn tại, nhưng nó giống như một object. Tên của các kiểu dữ liệu và cách chúng làm việc tương tự như trong Java, nhưng có một số khác biệt bạn cần phải biết:

- Không có việc tự động chuyển đổi giữa các kiểu numeric. Ví dụ, bạn không thể gán một biến `Int` qua `Double`. Cần phải chỉ rõ kiểu muốn chuyển đổi, sử dụng một trong các hàm có sẵn:

```kotlin
val i:Int=7
val d: Double = i.toDouble()
```

- Ký tự (Char) không thể trực tiếp sử dụng như numbers. Chúng ta có thể chuyển đổi chúng thành số nếu cần:

```kotlin
val c:Char='c'
val i: Int = c.toInt()
```

- Phép tính Bitwise có một chút khác biệt. Trong Android, chúng ta thường sử dụng bitwise cho các `flags`, vì vậy tôi sẽ lấy “and” và “or “ làm ví dụ:

```java
// Java
int bitwiseOr = FLAG1 | FLAG2;
int bitwiseAnd = FLAG1 & FLAG2;
```

```kotlin
// Kotlin
val bitwiseOr = FLAG1 or FLAG2
val bitwiseAnd = FLAG1 and FLAG2
```
>Có các phép tính khác với bitwise như `shl`, `shs`, `ushr`, `xor` hoặc `inv`. Bạn có thể tham khảo [official Kotlin reference¹⁴] để biết thêm thông tin chi tiết.

- Các ký tự có thể cho chúng ta biết thông tin về kiểu dữ liệu của no. Điều này không phải yêu cầu bắt buộc, trong thực tế phổ biến khi làm việc với Kotlin nó bỏ qua các loại biến (xíu nữa sẽ thấy), vì vậy chúng ta cần phải cung cấp một số thông tin để trình biên dịch biết:

```kotlin
val i = 12 // An Int
val iHex = 0x0f // An Int from hexadecimal literal
val l = 3L // A Long
val d = 3.5 // A Double
val f = 3.5F // A Float
```

- Một `String` có thể được truy cập như một array và có thể được lặp:

```kotlin
val s = "Example"
val c = s[2] // Đây là ký tự 'a'
// Lặp String
val s = "Example"
for(c in s){
    print(c)
}
```

[official Kotlin reference¹⁴]: http://kotlinlang.org/docs/reference/basic-types.html#operations