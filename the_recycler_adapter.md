# The Recycler Adapter

Chúng ta cần một adapter cho recycler. Tôi đã [nói về RecyclerView trên blog¹³](http://antonioleiva.com/recyclerview/) một thời gian trước, nó có thể giúp bạn nếu bạn chưa sử dụng qua.

Các views được sử dụng cho `RecyclerView` adapter hiện tại chỉ là các `TextView`, và danh sách text chúng ta sẽ tạo như sau. Thêm Kotlin file đặt tên là `ForecastListAdapter.kt`, có nội dung như bên dưới:
```kotlin
class ForecastListAdapter(val items: List<String>) :
        RecyclerView.Adapter<ForecastListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(TextView(parent.context))
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = items.[position]
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(val textView: TextView) : RecyclerView.ViewHolder(textView)
}
```

Lần nữa, chúng ta có thể truy cập context và text như là properties. Bạn có thể làm như bình thường (sử dụng getters và setters), nhưng bạn sẽ nhận được warning từ trình biên dịch. Phần kiểm tra này có thể được disable nếu bạn thích viết kiểu Java. Một khi bạn sử dụng qua properties bạn sẽ yêu cmnl.

>Visibility mặc định is `public`
>>Nếu không có các visibility modifier nào được áp dụng, các class, function hoặc properties mặc định là `public`. Bạn có thể viết nó ra, nhưng trình biên dịch sẽ hiển thị warninng, rằng nó không cần thiết.

Trở lại `MainActivity`, bây giờ chỉ cần tạo danh sách strings và gán cho adapter:
```kotlin
private val items = listOf(
    "Mon 6/23 - Sunny - 31/17",
    "Tue 6/24 - Foggy - 21/8",
    "Wed 6/25 - Cloudy - 22/17",
    "Thurs 6/26 - Rainy - 18/11",
    "Fri 6/27 - Foggy - 21/10",
    "Sat 6/28 - TRAPPED IN WEATHERSTATION - 23/18",
    "Sun 6/29 - Sunny - 20/7"
    )

override fun onCreate(savedInstanceState: Bundle?) {
    ...
    val forecastList = findViewById(R.id.forecast_list) as RecyclerView
    forecastList.layoutManager = LinearLayoutManager(this) 
    forecastList.adapter = ForecastListAdapter(items)
}
```

>List creation
>>Mặc dù tôi sẽ nói về collections sau trong quyển sách này, tôi chỉ muốn giải thích bây giờ rằng bạn có thể tạo các constant list (chúng ta sẽ thấy là chúng sẽ không bị thay đổi) bằng cách sử dụng hàm `listOf`. Nó nhận vào một `vararg` các items của bất kỳ kiểu dữ liệu và chuyển nó về kiểu của kết quả trả về.

>>Có nhiều functions thay thế như `setOf`, `mutableListOf` hoặc `hashSetOf`

  Tôi cũng move một vài class qua package mới, để cải thiện cấu trúc thư mục.
  Chúng tôi đã xem xét rất nhiều ý tưởng mới với ít code nhất, vì vậy tôi sẽ bao quát phần này ở chương sau. Chúng ta không thể tiếp tục cho đến khi chúng ta học được một số khái niệm quan trọng liên quan đến kiểu dữ liệu căn bản, varaibales, và properties.