# How to create a custom delegate

Let’s say we want, for instance, to create a non-nullable delegate that can be only assigned once. Second time it’s assigned, it will throw an exception.

Kotlin library provides a couple of interfaces our delegates must implement: `ReadOnlyProperty` and `ReadWriteProperty`. The one that should be used depends on whether the delegated property is `val` or `var`.

The first thing we can do is to create a class that extends `ReadWriteProperty`:

```kotlin
private class NotNullSingleValueVar<T>() : ReadWriteProperty<Any?, T> {

        override fun getValue(thisRef: Any?, property: KProperty<*>): T {
            throw UnsupportedOperationException()
        }

        override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        }
}
```

This delegate can work over any non-nullable type. It will receive a reference of an object of any type, and use T as the type of the getter and the setter. Now we need to implement the methods.

- The getter will return a value if it’s assigned, otherwise it will throw an exception.
- The setter will assign the value if it is still null, otherwise it will throw an exception.

```kotlin
private class NotNullSingleValueVar<T>() : ReadWriteProperty<Any?, T> {
    private var value: T? = null
    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value ?: throw IllegalStateException("${desc.name} " +
                "not initialized")
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = if (this.value == null) value
        else throw IllegalStateException("${desc.name} already initialized")
    }
}
```

Now let’s create an object with a function that provides your new delegate:

```kotlin
object DelegatesExt {
    fun notNullSingleValue<T>():
            ReadWriteProperty<Any?, T> = NotNullSingleValueVar()
}
```