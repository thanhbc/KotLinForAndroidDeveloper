# Settings Screen

Until now, we’ve been using a default city to implement the App, but it’s time add the ability to select a city. Our App needs a settings section where the user can change the city.

We are going to stick to the zip code to identify the city. A real App would probably need more information, because a zip code by itself doesn’t identify a city in the whole world. But we at least will show a city around the world that uses the zip code we are defining in settings. This will be a good example to explain an interesting way to deal with preferences.