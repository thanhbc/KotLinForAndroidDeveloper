# How to use Kotlin Android Extensions

If you remember, the project is already prepared to use Kotlin Android Extensions. When we were creating the project, we already added the dependency to app `build.gradle`:

```gradle
buldscript{
    repositories {
        jcenter()
    }
    dependencies {
        classpath "org.jetbrains.kotlin:kotlin-android-extensions:$kotlin_version"
    }
}
```

The only thing required by the plugin is the addition of a special “synthetic” `import` to the class that makes use of this feature. We have a couple of ways to use it:


### Android Extensions for Activities or Fragments

This is the regular way to use it. The views can be accessed as if they were properties of the activity or fragment. The names of the properties are the ids of the views in the XML.

The `import` we need to use will start with `kotlin.android.synthetic` plus the name of the XML we want to bind to the activity. We also have to specify the build variant:

```kotlin
import kotlinx.android.synthetic.main.activity_main.*
```

From that moment, we can access the views after `setContentView` is called. New Android Studio versions are adding nested layouts to default activity templates, by using `include` tag. It’s important to know that we’ll need to add a synthetic import for any XMLs we use:

```kotlin
import kotlinx.android.synthetic.activity_main.*
import kotlinx.android.synthetic.content_main.*
```

### Android Extensions for Views
The previous use is rather restrictive, because there are many other parts of the code where we could need to access the views inside an XML. For example, a custom view or an adapter. For these cases, there is an alternative which will bind the views of the XML to another view. The only difference is the required `import`:

```kotlin
import kotlinx.android.synthetic.main.view_item.view.*
```

If we were in an adapter, for instance, we could now access the properties from the inflated views:

```kotlin
view.textView.text = "Hello"
```

