# Enum classes

Kotlin also provides a way to implement **enums**:
```kotlin
enum class Day {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
    THURSDAY, FRIDAY, SATURDAY
}
```
Enums can have parameters:
```kotlin
enum class Icon(val res: Int) {
    UP(R.drawable.ic_up),
    SEARCH(R.drawable.ic_search),
    CAST(R.drawable.ic_cast)
}

val searchIconRes = Icon.SEARCH.res
```

Enums can be requested by the `String` that matches a name, and we can also get an `Array` which includes all the values of an enum, so that we can iterate over them.
```kotlin
val search: Icon = Icon.valueOf("SEARCH")
val iconList: Array<Icon> = Icon.values
```

Besides, every Enum constant has functions to obtain its name and the position in the declaration:
```kotlin
val searchName: String = Icon.SEARCH.name
val searchPosition: Int = Icon.SEARCH.ordinal
```
Enums implement Comparable based on the constants ordinal, so it’s easy to compare them.