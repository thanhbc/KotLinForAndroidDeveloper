# Conclusion
Thanks for reading this book. Throughout this pages, we’ve learned Kotlin by implementing an Android App as an example. The weather App was a good example to implement the basic features most Apps need: a master/detail UI, communication with an API, database storage, shared preferences…

The good thing about this method is that you have learned the most important Kotlin concepts while using them. In my opinion, new knowledge is more easily absorbed when it is put into action. It was my main goal, because reference books are usually a nice tool to solve some punctual doubts, but they are hard to read from the beginning to the very end. Besides, as the examples are usually out of a bigger context, it’s difficult to understand which kind of problems those features solve.

And that was, in fact, the other goal of the book: to show you real problems we face in Android and how they can be solved using Kotlin. Any Android developer finds a lot of questions when dealing with asynchrony, databases, or has to deal with really verbose listeners or activity navigations. By using a real App as an example, we have answered many of these questions while learning new language or libraries features.

I hope these goals were achieved, and I really hope that you not only learned Kotlin but also enjoyed reading this book. I’m convinced that Kotlin is right now the best alternative to Java for Android developers, and that we’ll see a boost during next months. You will be one of the first in the boat when that happens, and will be perfectly positioned to be a reference in your circle.

This book is finished, but it doesn’t mean it’s dead. I’ll keep updating it to the latest versions of Kotlin, reviewing and improving it based on your comments and suggestions. Feel free to write me about it at any moment and tell me what you think, the errors you find, concepts that are not clear enough or whatever concern you may have.

It’s been an amazing journey during the months I’ve been writing this book. I have learned a lot too, so thanks again for helping ‘Kotlin for Android Developers’ to become a reality.

Best,

Antonio Leiva

- Site: [antonioleiva.com³⁴]
- Email: [contact@antonioleiva.com³⁵]
- Twitter: [@lime_cl³⁶]
- Google+: [+AntonioLeivaGordillo³⁷]

[antonioleiva.com³⁴]:http://antonioleiva.com
[contact@antonioleiva.com³⁵]:mailto:contact@antonioleiva.com
[@lime_cl³⁶]:http://twitter.com/lime_cl
[+AntonioLeivaGordillo³⁷]:http://plus.google.com/+AntonioLeivaGordillo