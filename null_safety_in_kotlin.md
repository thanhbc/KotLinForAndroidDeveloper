# Null safety in Kotlin

Null safety is one of the most interesting features about Kotlin if you are currently working with Java 7. But as you have seen during this book, it’s so implicit in the language we hardly had to worry about it until the previous chapter.

Being `null` considered the [billion-dollar mistake by its own creator²³], it’s true that we sometimes need to define whether a variable contains a value or not. In Java, though annotations and IDEs are helping a lot these days, we can still do something like:
```kotlin
Forecast forecast = null;
forecast.toString();
```

This code will perfectly compile (you may get a warning from the IDE), and when it runs, it will obviously throw a `NullPointerException`. This is really unsafe, and as we can think we should be able to have everything under control, as the code grows we’ll start losing track of the things that could be null. So we end up with lots of `NullPointerExceptions` or lots of nullity checks (probably a mix of both).

[billion-dollar mistake by its own creator²³]:https://en.wikipedia.org/wiki/Tony_Hoare