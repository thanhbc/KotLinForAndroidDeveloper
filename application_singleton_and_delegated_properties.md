# Application Singleton and Delegated Properties

We are going to implement a database soon and, if we want to keep our code simple and our app in independent layers (instead of everything added to our activity), we’ll need to have an easier access to the application context.