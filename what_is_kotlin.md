# What is Kotlin?

Kotlin, như đã mô tả lúc trước, là một ngôn ngữ chạy trên JVM được phát triển bởi [JetBrains⁵](https://www.jetbrains.com), một công ty được biết đến vì tạo ra IntelliJ IDEA, một IDE mạnh mẽ để phát triển Java. Android Studio, IDE chính thức của Android, dựa trên IntelliJ.

Kotlin được tạo ra với các nhà phát triển Java, và với IntelliJ là IDE phát triển chính. Và có 2 điểm nổi bật rất thú vị cho các nhà phát triển Android:
* **Kotlin rất trực quan và dễ học đối với Java developers**. Đa số các phần của ngôn ngữ điều tương tự như những gì chúng ta đã biết, và sự khác biệt cơ bản trong khái niệm có thể học nhanh chóng.
* **Chúng tôi hoàn toàn tích hợp với IDE miễn phí**. Android Studio có thể hiểu, biên dịch và chạy Kotlin code. Và ngôn ngữ này được hỗ trợ bởi công ty đã phát triển ra IDE, vì vậy chúng ta, Android developers là những công dân đầu tiên.

Nhưng điều này chỉ liên quan đến việc làm thế nào để tích hợp ngôn ngữ này vào công cụ của chúng ta. Ưu điểm của ngôn ngữ này là gì khi so sánh với Java 7?

* **Có ý nghĩa hơn**: đây là một trong những điều quan trọng nhất. Bạn có thể viết nhiều hơn với vài dòng code.
* **An toàn hơn**: Kotlin là null safe, nghĩa là khi chúng ta xử lý với các trường hợp có thể trong thời gian khi biên dịch, để tránh các exceptions trong thời gian thực thi. Chúng ta cần xác định rõ ràng đối tượng có thể bị null, và sau đó kiểm tra xem có bị null không trước khi sử dụng. Bạn sẽ tiếp kiệm được rất nhiều thời gian debug null pointer exception và sửa bugs này.
* **Nó functional**: Kotlin cơ bản là ngôn ngữ hướng đối tượng, không phải ngôn ngữ hướng chức năng (functional). Tuy nhiêm, cũng như các ngôn ngữ hiện đại khác, nó sử dụng khái niệm từ lập trình hướng chức năng (functional programming), như lambda expressions, để giải quyết các vấn đề theo hướng đơn giản nhất. Điểm nổi bật khác là cách nó làm việc với collections.
* **Có thể sử dụng để mở rộng chức năng**: Điều này nghĩa là chúng ta có thể mở rộng bất kỳ lớp nào với chức năng mới mà thậm chí không cần phải truy cập vào mã nguồn của nó.
* **Có tính tương thích cao**: Bạn có thể tiếp tục sử dụng những thư viện cũng như code được viết bằng Java, bởi vì tính tương thích giữa 2 ngôn ngữ. Nó thậm chí có thể tạo mixed project, với các file Kotlin và Java cùng tồn tại.



---
⁵https://www.jetbrains.com/
