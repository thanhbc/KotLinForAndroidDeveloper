# If Expression

Almost everything in Kotlin is an expression, which means it returns a value. `If` conditions are not an exception, so though we can use `if` as we are used to do:
```kotlin
if(x>0){
    toast("x is greater than 0")
}else if(x==0){ 
    toast("x equals 0")
}else{
    toast("x is smaller than 0")
}
```

We can also assign its result to a variable. We’ve used it like that several times in our code:

```kotlin
val res = if (x != null && x.size() >= days) x else null
```

This also implies we don’t need a ternary operation similar to the Java one, because we can solve it easily with:
```kotlin
val z = if (condition) x else y
```

So the `if` expression always returns a value. If one of the branches returns Unit, the whole expression will return Unit, which can be ignored, and it will work as a regular Java `if` condition in that case.