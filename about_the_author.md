# About the author

Antonio Leiva là một Android Engineer người dành thời gian để tìm hiểu về Android theo nhiều cách và sau đó viết về nó. Anh ấy viết blog tại [antonioleiva.com¹](http://antonioleiva.com) về những chủ đề khác nhau liên quan đến Android development.

Antonio khởi đầu là một nhà tư vấn về CRM technologies, nhưng sau đó, tìm kiếm đam mê thực sự của anh ấy, anh ấy đã khám phá ra thế giới Android. Sau khi có được một số kinh nghiệm trên nền tảng tuyệt vời này, anh ấy bắt đầu cuộc hành trình mới tại công ty mobile, nơi mà anh ấy led một số dự án cho những công ty quan trọng của Tây Ban Nha.

Anh ấy hiện tại là Android Engineer tại [Plex²](http://plex.tv), nơi anh anh giữ vai trò quan trong trọng việc thế kế UX cho các ứng dụng Android.

Bạn có thể tìm Antonio trên Twitter [@lime_cl³](https://twitter.com/lime_cl) hoặc Google+  [+AntonioLeivaGordillo⁴](http://plus.google.com/+AntonioLeivaGordillo‘).

---

¹http://antonioleiva.com 

²http://plex.tv

³https://twitter.com/lime_cl

⁴http://plus.google.com/+AntonioLeivaGordillo