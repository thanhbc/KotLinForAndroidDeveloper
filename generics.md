# Generics

Generic programming consists of writing algorithms without the need of specifying the exact type the code is going to use. That way, we can create functions or types that only differ in the set of types they use, improving code reusability. These units of code are known as generics, and they exist in many languages, including Java and Kotlin.

In Kotlin, generics are even more important, because the high presence of regular and extension functions will increment the amount of times that generics are of some use for us. Though we’ve been using them blindly throughout the book, generics are usually one of the trickiest parts of any language, so I’m trying to explain it in the simplest possible way so that main ideas are clear enough.