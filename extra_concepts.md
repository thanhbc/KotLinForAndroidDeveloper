# Extra concepts

Through this book, we’ve talked about most important concepts of Kotlin language. But we didn’t use some of them when implementing the App, and I wouldn’t want to let them out of these pages. In this chapter, I will review some unrelated features that you could need when you develop your next Android App using Kotlin.