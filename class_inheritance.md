# Class inheritance

Theo mặc định, một class có thể kế thừa từ `Any` (tương tự như Java `Object`), nhưng chúng ta có thể mở rộng từ bất kỳ class khác. Các class mặt định là class đóng (closed by default) (final), vì vậy chúng ta chỉ có thể mở rộng nếu như nó được khai báo `open` hoặc `abstract`:

```kotlin
open class Animal(name: String)
class Person(name: String, surname: String) : Animal(name)
```
Lưu ý rằng khi sử dụng single constructors, chúng ta cần phải xác định paramenters cần sử dụng cho parent constructor. Điều này giống với việc gọi `super()` trong Java.