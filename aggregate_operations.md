# Aggregate operations


### any
Returns `true` if at least one element matches the given predicate.
```kotlin
val list = listOf(1, 2, 3, 4, 5, 6)
assertTrue(list.any { it % 2 == 0 })
assertFalse(list.any { it > 10 })
```

### all
Returns `true` if all the elements match the given predicate.
```kotlin
assertTrue(list.all { it < 10 })
assertFalse(list.all { it % 2 == 0 })
```

### count
Returns the number of elements matching the given predicate.
```kotlin
assertEquals(3, list.count { it % 2 == 0 })
```

### fold
Accumulates the value starting with an initial value and applying an operation from the first to the last element in a collection.
```kotlin
assertEquals(25, list.fold(4) { total, next -> total + next })
```

### foldRight
Same as `fold`, but it goes from the last element to first.
```kotlin
assertEquals(25, list.foldRight(4) { total, next -> total + next })
```

### forEach
Performs the given operation to each element.
```kotlin
list.forEach { println(it) }
```

### forEachIndexed
Same as `forEach`, though we also get the index of the element.
```kotlin
list.forEachIndexed { index, value
        -> println("position $index contains a $value") }
```

### max
Returns the largest element or `null` if there are no elements.
```kotlin  
assertEquals(6, list.max())
```

### maxBy
```kotlin
// The element whose negative is greater
assertEquals(1, list.maxBy { -it })
```

### min
Returns the smallest element or `null` if there are no elements.
```kotlin
assertEquals(1, list.min())
```

### minBy
Returns the first element yielding the smallest value of the given function or `null` if there are no elements.
```kotlin
// The element whose negative is smaller
assertEquals(6, list.minBy { -it })
```


### none

Returns `true` if no elements match the given predicate.
```kotlin
// No elements are divisible by 7
assertTrue(list.none { it % 7 == 0 })
```


### reduce
Same as `fold`, but it doesn’t use an initial value. It accumulates the value applying an operation from the first to the last element in a collection.
```kotlin
assertEquals(21, list.reduce { total, next -> total + next })
```

### reduceRight
Same as `reduce`, but it goes from the last element to first.
```kotlin
assertEquals(21, list.reduceRight { total, next -> total + next })
```

### sumBy
Returns the sum of all values produced by the transform function from the elements in the collection.
```kotlin
assertEquals(3, list.sumBy { it % 2 })
```
























