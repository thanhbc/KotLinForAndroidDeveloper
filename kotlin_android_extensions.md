# Kotlin Android Extensions

Another interesting plugin the Kotlin team has developed to make Android development easier is called **Kotlin Android Extensions**. Currently it only includes a view binder. The plugin automatically creates a set of properties that give direct access to all the views in the XML. This way we don’t need to explicitly find all the views in the layout before starting using them.

The names of the properties are taken from the ids of the views, so we must be careful when choosing those names because they will be an important part of our classes. The type of these properties is also taken from the XML, so there is no need to do any extra castings.

The good thing about Kotlin Android Extensions is that it doesn’t add any extra libraries to our code. It just consists of a plugin that generates the code it needs to work only when it’s required, just by using the standard Kotlin library.

How does it work under the hood? The plugin substitutes any properties call into a function that requests the view, and a caching function that prevents from having to find the view every time a property is called. Be aware that this caching mechanism only works if the receiver is an Activity or a Fragment. If it’s used in an extension function, the caching will be skipped, because it could be used in an activity the plugin is not able to modify, so it won’t be able to add the caching function.