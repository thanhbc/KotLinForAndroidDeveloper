# Is this book for you

Quyển sách này được viết để hữu ích với các nhà phát triển Android nào quan tâm đến ngôn ngữ Kotlin.

Quyển sách này phù hợp với bạn nếu bạn đang ở trong những tình huống sau:
* Bạn muốn có một số kiến thức căn bản về Android Development và Android SDK.
* Bạn muốn học để phát triển các ứng dụng Android sử dụng Kotlin qua ví dụ.
* Bạn cần hướng dẫn làm sao để giải quyết những thánh thức thường thấy mà một nhà phát triển Android gặp hàng ngày, bằng cách sử dụng một ngôn ngữ ngắn gọn và có ý nghĩa hơn.

Mặc khác, quyển sách này có thể không phù hợp với bạn. Đây là những thứ bạn sẽ không tìm được:
* Đây không phải là Kinh thánh Kotlin. Tôi sẽ giải thích tất cả các thứ căn bản, và thậm chí là những ý tưởng phức tạp phát sinh trong quá trình, chỉ khi nào chúng ta cần nó. Vì vậy bạn sẽ học bằng ví dụ chứ không phải ngược lại.
* Tôi sẽ không giải thích làm thế nào để phát triển ứng dụng Android. Bạn sẽ không cần kiến thức sâu sắc về platform, nhưng bạn cần biết một số cái cơ bản, nhưng kiến thức về Android Studio, Gradle, Java programming và Android SDK. Bạn có thể học được vài thứ mới trong suốt quá trình!
* Đây không phải hướng dẫn để học functional programming. Dĩ nhiên, tôi sẽ giải thích bạn cần gì, Java 7 không hoàn toàn là functional, nhưng tôi sẽ không đi sâu vào chủ đề functional.

