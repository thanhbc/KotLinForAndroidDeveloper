# Create the project in Android Studio


Trước hết, mở Android Studio và chọn `Create new Project`. Lúc này sẽ yêu cầu nhập tên ứng dụng, bạn muốn nhập gì cũng được: Vì dụ như `WeatherApp` chẳng hạn. Sau đó bạn cần nhập Company Domain. Nếu bạn không release app thì phần này không quan trong lắm, nhưng nếu bạn có một domain, thì bạn có thể dùng nó. Cũng như chọn chỗ lưu project.

Bước tiếp theo, bạn sẽ được hỏi về phiên bản API tối thiếu. Chúng ta sẽ chọn API 15, bởi vì những thư viện chúng ta sẽ sử dụng cần tối thiểu API 15. Bạn cũng sẽ hỗ trợ được nhiều người dùng Android. Hiện tại đừng chọn các platform khác ngoại trừ `Phone and Tablet`.

Cuối cùng, chúng ta sẽ được yêu cầu chọn activity template để bắt đầu. Chúng ta chọn `Add no Activity` và bắt đầu từ đầu (tư tưởng tốt nhất để bắt đầu với Kotlin), nhưng chúng ta sẽ chọn `Empty Activity` vì chút nữa tôi sẽ cho bạn xem một chức năng thú vị từ Kotlin plugin.

Đừng xoắn về tên của activities, layouts, etc. mà bạn sẽ thấy ở màn hình tiếp theo.
Chúng ta sẽ thay đổi chúng nếu cần. Nhấn `Finish` và đợi Android Studio khởi tạo project.