# Creating an SQLiteOpenHelper

As you may know, Android uses SQLite as a database management system. SQLite is a database embedded into the App, and it’s really lightweight. That’s why it is a good option for mobile Apps.

However, the API to work with databases in Android is quite raw. You’ll see you need to write many SQL sentences and map your objects into `ContentValues` or from `Cursors`. Thankfully, by using a mix of Kotlin and Anko, we are simplifying this task a lot.

Of course, there are many libraries to work with databases in Android, and all of them work in Kotlin thanks to its interoperability. But it’s possible you don’t need them for simple databases, as we are going to see in a minute.