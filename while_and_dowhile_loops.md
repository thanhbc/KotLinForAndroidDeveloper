# While and do/while loops

You can keep using while loops too, though it won’t be very common either. There are usually simpler and more visual ways to resolve a problem. A couple of examples:

```kotlin
while(x > 0){ 
    x--
}

do{
    val y = retrieveData()
} while (y != null) // y is visible here!
```