# Properties

Properties tương tự với fields trong Java, nhưng mạnh mẽ hơn. Properties sẽ thực hiện công việc của field cộng với getter và setter. Hãy xem ví dụ so sánh sự khác biệt. Đây là đoạn code trong Java có thể truy cập và thay đổi field một cách an toàn:
```kotlin
public class Person {
    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) { 
        this.name = name;
    }
}
...
Person person = new Person();
person.setName("name");
String name = person.getName();
```

Trong Kotlin, chỉ cần property:
```kotlin
public class Person {
    var name: String = ""
}

...

val person = Person()
person.name = "name"
val name = person.name
```

Nếu không có gì được xác định,  property sẽ sử dụng getter và setter mặc định. Dĩ nhiên là nó có thể được chỉnh sửa để chạy bất kỳ đoạn code tuỳ biến nào bạn cần, mà không cần phải thay đổi code cũ:
```kotlin
public classs Person {
    var name: String = ""
        get() = field.toUpperCase()
        set(value){
            field = "Name: $value"
        }
}
```
Nếu property cần truy cập vào giá trị của nó với custom getter và setter (như trong trường hợp này), nó cần phải được khởi tạo bởi **backing field**. Nó có thể được truy cập bằng cách sử dụng field, một từ đảo ngược và sẽ được tạo tự động bởi trình biên dịch khi tìm thấy rằng nó đang được sử dụng. Lưu ý rằng nếu chúng ta sử dụng property trực tiếp, chúng ta có thể sử dụng setter và getter, và không gán trực tiếp. backing field chỉ có thể được sử dụng bên trong property accessors.

Như đã đề cập ở một vài chương trước, khi thao tác với Java code Kotlin sẽ cho phép sử dụng property syntax nơi mà getter và setter được định nghĩa trong Java. Trình biên dịch chỉ link đến getters và setters ban đầu, vì vậy không có vấn đề gì về performance khi sử dụng mapped properties.