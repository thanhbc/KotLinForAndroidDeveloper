# Revising our code

We’ve already been making use of the `public` default refactor, but there are many other details we could change. For instance, in `RequestForecastCommand`, the property we create from the `zipCode` constructor parameter could be private.

```kotlin
class RequestForecastCommand(private val zipCode: String)
```

The thing is that as we are making use of immutable properties, the `zipCode` value can only be requested, but not modified. So it is not a big deal to leave it as public, and the code looks cleaner. If, when writing a class, you feel that something shouldn’t be visible by any means, feel free to make it `private`.

Besides, in Kotlin we don’t need to specify the return type of a function if it can be computed by the compiler. An example of how we can get rid of the returning types:
```kotlin
data class ForecastList(...) {
    fun get(position: Int) = dailyForecast[position]
    fun size() = dailyForecast.size()
}
```

The typical situations where we can get rid of the return type are when we assign the value to a function or a property using `equals` (=) instead of writing a code block.

The rest of the modifications are quite straightforward, so you can check them in the repository.