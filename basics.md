# Basics

For instance, we can create a class which specifies a generic type:
```kotlin
class TypedClass<T>(parameter: T) {
    val value: T = parameter
}
```

This class now can be instantiated using any type, and the parameter will use the type in the definition. We could do:
```kotlin
val t1 = TypedClass<String>("Hello World!")
val t2 = TypedClass<Int>(25)
```
But Kotlin is all about simplicity and boilerplate reduction, so if the compiler can infer the type of the parameter, we don’t even need to specify it:
```kotlin
val t1 = TypedClass("Hello World!")
val t2 = TypedClass(25)
val t3 = TypedClass<String?>(null)
```

As the third object is receiving a null reference, the type still needs to be specified because it can’t be inferred.

We can go beyond and, as in Java, reduce the types that can be used in a generic class by setting it in the definition. For instance, if we want to restrict previous class to non-nullable types, we just need to do:

```kotlin
class TypedClass<T : Any>(parameter: T) { 
    val value: T = parameter
}
```

If you compile previous code, you will see that `t3` now throws an error. Nullable types are not allowed anymore. But restrictions can be obviously more strict. What if we want only classes that extend `Context?` Easy:

```kotlin
class TypedClass<T : Context>(parameter: T) { 
    val value: T = parameter
}

val t1 = TypedClass(activity)
val t2 = TypedClass(context)
val t3 = TypedClass(service)
```

Now every class which extends `Context` can be used as the type of our class. The rest of types are not allowed.

Of course, types are also allowed in functions. We can construct generic functions quite easily:

```kotlin
fun <T> typedFunction(item: T): List<T> {
    ...
}
```