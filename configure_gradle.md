# Configure Gradle
Kotlin plugin bao gồm công cụ có thể cấu hình Gradle cho chúng ta. Tuy nhiên tôi thích kiểm soát những gì mà tôi viết trên files Gradle, nếu không thì nó có thể bị rối. Dù sao thì, đó cũng là một ý tưởng tối để biết mọi thứ làm việc như thế nào trước khi sử dụng automatic tools, vậy chúng ta sẽ  làm bằng tay lúc này.

Đầu tiên, bạn cần chỉnh sửa file `build.gradle` cha của ứng dụng (parent build.gradle) như sau:

```groovy
buildscript {
    ext.support_version = '23.1.1'
    ext.kotlin_version = '1.0.0'
    ext.anko_version = '0.8.2'
    repositories {
        jcenter()
        dependencies {
            classpath 'com.android.tools.build:gradle:1.5.0'
            classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
        }
    }
}
allprojects {
    repositories {
        jcenter() 
    }
}
```
Như bạn có thể thấy, chúng ta tạo ra một variable để lưu Kotlin version. Kiểm tra version nào mới nhất khi bạn đang đọc những dòng này, có lẽ đã có version mới. Chúng ta cần version number ở nhiều chỗ, ví dụng như trong dependency mới bạn cần thêm vào Kotlin plugin. Bạn sẽ cần nó trong module `build.gradle`, nơi chúng ta sẽ sử dụng **Kotlin standard library**.

Chúng ta cũng làm tương tự với support library, cũng như Anko library. Bằng cách này, nó sẽ dễ dàng để thay đổi toàn bộ version trong 1 dòng, cũng như thêm vào các library mới sử dụng chung version mà không cần phải thay đổi version ở mọi nơi.


Chúng ta sẽ thêm các dependency vào **Kotlin standard library** và **Anko library** giống như **Kotlin** và **Kotlin Android Extensions plugins**.

```groovy
apply plugin: 'com.android.application'
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-android-extensions'
android {
    ...
}

dependencies {
    compile "com.android.support:appcompat-v7:$support_version"
    compile "org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version"
    compile "org.jetbrains.anko:anko-common:$anko_version"
}

buildscript {
    repositories {
jcenter() }
    dependencies {
      classpath "org.jetbrains.kotlin:kotlin-android-extensions:$kotlin_version"
    } 
}
```

Anko là thư viện sử dụng thế mạnh của Kotlin để đơn giản hoá một số tác vụ với Android. Chúng ta sẽ cần Anko vào những phần sau, tuy nhiên hiện tại thì chỉ cần thêm `anko-common`. Thư viện này được chia nhỏ nên chúng ta không cần phải include hết mọi thứ nếu không sử dụng.