# Click listener for ForecastListAdapter

In the previous chapter, I wrote the click listener the hard way on purpose to have a good context to develop this one. But now it’s time to put what you learnt into practice. We are removing the listener interface from the `ForecastListAdapter` and using a lambda instead:

```kotlin
public class ForecastListAdapter(val weekForecast: ForecastList,
                                 val itemClick: (Forecast) -> Unit)
                                 ```
The function will receive a forecast and return nothing. The same change can be done to the `ViewHolder`:  
```kotlin
class ViewHolder(view: View, val itemClick: (Forecast) -> Unit)
```

The rest of the code remains unmodified. Just a last change to `MainActivity`:

```kotlin
val adapter = ForecastListAdapter(result) { forecast -> toast(forecast.date) }
```

We can simplify the last line even more. In functions that only need one parameter, we can make use of the **_it_** reference, which prevents us from defining the left part of the function specifically. So we can do:

```kotlin
val adapter = ForecastListAdapter(result) { toast(it.date) }
```

                               