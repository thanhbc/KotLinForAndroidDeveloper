# Implementing SqliteOpenHelper

If you remember, Anko is divided into several libraries to be more lightweight. We already added `anko-common`, but we also need `anko-sqlite` if we want to use database features:

```kotlin
dependencies {
     ...
     compile "org.jetbrains.anko:anko-sqlite:$anko_version"
}
```
Our `SqliteOpenHelper` will basically manage the creation and upgrade of our database, and will provide the `SqliteDatabase` so that we can work with it. The queries will be extracted to another class:
```kotlin
class ForecastDbHelper() : ManagedSQLiteOpenHelper(App.instance,
        ForecastDbHelper.DB_NAME, null, ForecastDbHelper.DB_VERSION) {
    ...
}
```

We are using the `App.instance` we created in the previous chapter, as well as a database name and version. These values will be defined in the companion object, along with the helper single instance:

```kotlin
companion object {
    val DB_NAME = "forecast.db"
    val DB_VERSION = 1
    val instance: ForecastDbHelper by lazy { ForecastDbHelper() }
}
```

The `instance` property uses a `lazy` delegate, which means the object won’t be created until it’s used. That way, if the database is never used, we don’t create unnecessary objects. The regular `lazy` delegate is blocking in order to prevent the creation of several `instances` from different threads. This only would happen if two threads try to access the instance at the same time, which is difficult but it could happen depending on the type of App you are implementing. But the regular `lazy`delegate is thread safe.

In order to define the creation of the tables, we are required to provide an implementation of the `onCreate` function. When no libraries are used, the creation of the tables is done by writing a raw `CREATE TABLE` query where we define all the columns and their types. However, Anko provides a simple extension function which receives the name of the table and a set of `Pair` objects that identify the name and the type of the column:

```kotlin
db.createTable(CityForecastTable.NAME, true,
        Pair(CityForecastTable.ID, INTEGER + PRIMARY_KEY),
        Pair(CityForecastTable.CITY, TEXT),
        Pair(CityForecastTable.COUNTRY, TEXT))
```

- The first parameter is the name of the table.
- The second parameter, when set to true, will check if the table doesn’t exist before trying to create it.
- The third parameter is a `vararg` of `Pairs`. The `vararg` type also exists in Java, and it’s a way to pass a variable number of arguments of the same type to a function. The function will receive an array with the objects.

The types are from a special Anko class called `SqlType`, which can be mixed with `SqlTypeModifiers`, such as `PRIMARY_KEY`. The + operation is overloaded the same way we saw in chapter 11. This `plus` function will concatenate both values in a proper way returning a new special `SqlType`:
```kotlin
fun SqlType.plus(m: SqlTypeModifier) : SqlType {
    return SqlTypeImpl(name, if (modifier == null) m.toString()
            else "$modifier $m")
}
```

As you can see, it can also concatenate several modifiers.

But returning to our code, we can do it better. Kotlin standard library includes a function called `to` which, once more, shows the power of Kotlin to let us model our own language. It acts as an extension function for the first object and receives another object as parameter, returning a `Pair` object with them.

```kotlin
infix fun <A, B> A.to(that: B): Pair<A, B> = Pair(this, that)
```

Functions with one parameter that use the `infix` modifier can be used inline, so the result is quite clean:
```kotlin
val pair = object1 to object2
```

And this, applied to the creation of our tables:

```kotlin
db.createTable(CityForecastTable.NAME, true,
        CityForecastTable.ID to INTEGER + PRIMARY_KEY,
        CityForecastTable.CITY to TEXT,
        CityForecastTable.COUNTRY to TEXT)
```

This is how the whole method looks:

```kotlin
override fun onCreate(db: SQLiteDatabase) {
    db.createTable(CityForecastTable.NAME, true,
            CityForecastTable.ID to INTEGER + PRIMARY_KEY,
            CityForecastTable.CITY to TEXT,
            CityForecastTable.COUNTRY to TEXT)

    db.createTable(DayForecastTable.NAME, true,
            DayForecastTable.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            DayForecastTable.DATE to INTEGER,
            DayForecastTable.DESCRIPTION to TEXT,
            DayForecastTable.HIGH to INTEGER,
            DayForecastTable.LOW to INTEGER,
            DayForecastTable.ICON_URL to TEXT,
            DayForecastTable.CITY_ID to INTEGER)
}
```

We have a similar function to drop a table. `onUpgrade` will just delete the tables so that they are recreated. We are using our database just as a cache, so it’s the easiest and safest way to be sure the tables are recreated as expected. If we had important data to be kept, we’d need to improve `onUpgrade` code by doing the corresponding migration depending on the database version.

```kotlin
override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    db.dropTable(CityForecastTable.NAME, true)
    db.dropTable(DayForecastTable.NAME, true)
    onCreate(db)
}
```