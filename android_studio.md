# Android Studio

Điều đầu tiền bạn cần là phải cài đặt Android Studio. Như đã biết thì Android Studio là IDE Android chính thức, được công bố vào 2013 với bản preview và được release vào 2014.

Android Studio được implement như là một plugin qua [IntelliJ IDEA](https://www.jetbrains.com/idea), Java IDE được phát triển bởi [Jetbrains](https://www.jetbrains.com), cũng là công ty phát triển Kotlin. Vì vậy, bạn có thể cảm thấy mọi thứ được kết nối với nhau.

  Việc áp dụng Android Stuido là một thay đổi quan trọng cho các Android developers. Đầu tiên, bởi vì chúng ta bỏ Eclipse lại phía sau và chuyển sang một phần mềm được thiết kế đặt biệt cho Java developers, mang lại cho chúng ta tương tác hoàn hảo với ngôn ngữ. Chúng ta hưởng thụ các chức năng tuyệt vời, smart code completion nhanh và ấn tượng, công cụ analysing và refactor mạnh mẽ so với những cái khác.
  
  Thứ hai, Gradle trở thành build system cho Android, nghĩa là toàn bộ các vấn đề liên quan đến version building và deploy. Hai trong số những chức năng nổi trội là build systems và flavours, giúp bạn tạo ra được những phiên bản khác nhau của app (thậm chí là những app khác nhau) theo cách đơn giản nhất khi sử dụng cùng code base.
  
  Nếu bạn vẫn còn sử dụng Eclipse, tôi e là bạn cần phải chuyển sang Android Studio nếu bạn muốn theo dõi quyển sách này. Kotlin team đang tạo một plugin cho Eclipse, tuy nhiên nó vẫn còn khá xa so với Android Studio, và việc tích hợp vào sẽ không lúc nào cũng suôn sẻ. Bạn cũng sẽ cần phải tìm hiểu xem bạn đang thiếu thứ gì trước khi có thể sử dụng nó.
  
  Tôi sẽ không cover cách sử dụng Android Studio hoặc [Gradle](https://gradle.org/) bởi vì quyển sách này không nhắm đến việc đó, nhưng nếu nhưng bạn chưa sử dụng những công cụ này trước đây, không phải xoắn. Tôi chắc là bạn sẽ có thể follow theo quyển sách này và học căn bản luôn.
  
  Tải về [Android Studio từ trang chính thức](https://developer.android.com/sdk/index.html) nếu như bạn vẫn chưa có nó.