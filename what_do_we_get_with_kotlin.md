# What do we get with Kotlin?

Không cần đi quá sâu vào ngôn ngữ Kotlin (chúng ta sẽ học mọi thứ về nó trong quyển sách này), có một số chức năng thú vị mà chúng ta bỏ lỡ trong Java:


##Expresiveness (Ý nghĩa?)

Với Kotlin, sẽ dễ dàng hơn trong việc tránh boilerplate bởi vì trong hầu hết các trường hợp mặc định điều được cover bởi ngôn ngữ. Ví dụ, trong Java, nếu chúng ta muốn tạo một  lớp data, chúng ta sẽ cần viết (hoặc ít nhất là generate) đống bên dưới:
```java
public class Artist {
    private long id;
    private String name;
    private String url;
    private String mbid;

    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMbid() {
        return mbid;
    }

    public void setMbid(String mbid) {
        this.mbid = mbid;
    }

    @Override public String toString() {
        return "Artist{" +
          "id=" + id +
          ", name='" + name + '\'' +
          ", url='" + url + '\'' +
          ", mbid='" + mbid + '\'' +
          '}';
    }
}
```
Với Kotlin, bạn chỉ cần sử dụng một lớp data:
```kotlin
data class Artist(
    var id: Long,
    var name: String,
    var url: String,
    var mbid: String)
```
Lớp data này sẽ tự động tạo ra các truy xuất các field và property, cũng như một số method hữu ích như `toString()`.

## Null Safety
Khi chúng ta sử dụng Java để phát triển, phần lớn code của chúng ta đều defensive. Chúng ta cần kiểm tra liên tục xem có bị null trước khi sử dụng nếu chúng ta không muốn gặp chú em *NullPointerException*. Kotlin, cũng như các ngôn ngữ hiện đại khác, là null safe bởi vì chúng ta cần xác định rõ object có thể bị null khi sử dụng safe call operator (được viết là ?).

Chúng ta có thể làm vài thứ như vậy:
```kotlin
// Cái này sẽ không complie. Artist không được null 
var notNullArtist: Artist = null

// Artist có thể null
var artist: Artist? = null

// Không complie , artist có thể bị null，chúng ta cần xử lý cái này
artist.print()

// chỉ được thực thi nếu artist !=null 
artist?.print()

// trường hợp này không cần sử dụng safe call operator vì trước đó đã check null 
if (artist != null) {
  artist.print()
}

// Chỉ sử dụng khi chúng ta chắc chắn rằng nó ko bị null. Hoặc sẽ throw exception 
artist!!.print()

// sử dụng Elvis operator để thay thế trong trường hợp object bị null
val name = artist?.name ?: "empty"
```

## Extension functions (mở rộng chức năng)
Chúng ta có thể thêm các chức năng mới vào bất kỳ class nào. Nó dễ đọc hơn so với các lớp utility mà chúng ta có trong các projects. Chúng ta có thể thêm method mới cho fragments để hiển thị một toast, ví dụ: 
```kotlin
fun Fragment.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) { 
    Toast.makeText(getActivity(), message, duration).show()
}
```
Bây giờ chúng ta chỉ cần 
```kotlin
fragment.toast("Hello world!")
```

## Functional support (Lambdas)
Chuyện gì sẽ xảy ra, nếu như thay vì implement một anomymous class mỗi khi chúng ta cần implement click listener, chúng ta có thẻ chỉ cần định nghĩa chúng ta muốn làm gì? Chúng ta có thể làm điều đó. Đây ( và nhiều thứ thú vị khác) là những gì chúng ta phải cám ơn lambdas:
```kotlin
view.setOnClickListener { toast("Hello world!") }
```

Đây chỉ là mộ phần nhở mà Kotlin có thể làm để rút gọn code của bạn. Bây giờ bạn đã biết một số chức năng thú vị của ngôn ngữ rồi, bạn có lẻ không cần nó. Nhưng nếu bạn tiếp tục, chúng ta sẽ đi vào thực hành ngay chương sau.





