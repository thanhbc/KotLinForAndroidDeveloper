# Converting json to data classes

Now that we know how to create data classes, we are ready to start parsing data. In the `data` package, create a new file called `ResponseClasses.kt`. If you open the url we used in chapter 8, you can see the structure of the json file. It basically consists of an object which contains a city, and a list of forecast predictions. The city has an id, a name, its coordinates and the country it belongs to. Each forecast comes with a good set of information such as the date, different temperatures, and a weather object with the description and an id for an icon, for instance.

In our current UI we are not going to use all this data. However, we’ll parse everything down to classes, in case it is of some use in the future. These are the data classes we need:

```kotlin
data class ForecastResult(val city: City, val list: List<Forecast>)
data class City(val id: Long, val name: String, val coord: Coordinates,
                val country: String, val population: Int)
data class Coordinates(val lon: Float, val lat: Float)
data class Forecast(val dt: Long, val temp: Temperature, val pressure: Float,
                    val humidity: Int, val weather: List<Weather>,
                    val speed: Float, val deg: Int, val clouds: Int,
                    val rain: Float)
data class Temperature(val day: Float, val min: Float, val max: Float,
                       val night: Float, val eve: Float, val morn: Float)
data class Weather(val id: Long, val main: String, val description: String,
                 val icon: String)
```
As we are using Gson to parse the json to our classes, the properties must have the same name as the ones in the json, or specify a serialised name. A good practice explained in most software architectures is to use different models for the different layers in our app to decouple them from each other. So I prefer to simplify the declaration of these classes, because I’ll convert them before being used in the rest of the app. The names of the properties here are exactly the same as the names in the json response.

Now, the `Request` class needs some modifications in order to return the parsed result. It will also receive only the `zipcode` of the city instead of the complete url, so that it becomes more readable. For now, the static url will belong to a **companion object**. Maybe we need to extract it later if we create more requests against another endpoint of the API.

>Companion objects
>>Kotlin allows to declare objects to define static behaviours. In Kotlin, we can’t create static properties or functions, but we need to rely on objects. However, these objects make some well known patterns such as `Singleton` very easy to implement.

>>If we need some static properties, constants or functions in a class, we can use a **companion object**. This object will be shared among all instances of the class, the same as a static field or method would do in Java.

Check the resulting code:

```kotlin
public class ForecastRequest(val zipCode: String) {
    companion object {
        private val APP_ID = "15646a06818f61f7b8d7823ca833e1ce"
        private val URL = "http://api.openweathermap.org/data/2.5/" +
                "forecast/daily?mode=json&units=metric&cnt=7"
        private val COMPLETE_URL = "$URL&APPID=$APP_ID&q="
    }

    fun execute(): ForecastResult {
        val forecastJsonStr = URL(COMPLETE_URL + zipCode).readText()
        return Gson().fromJson(forecastJsonStr, ForecastResult::class.java)
    }
}
```
Remember you need to add Gson library to `build.gradle` dependencies:

```groovy
compile "com.google.code.gson:gson:2.4"
```
