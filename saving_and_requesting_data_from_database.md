# Saving and requesting data from database

A previous chapter covered the creation of a `SQLiteOpenHelper`, but now we need a way to use it to persist our data into the database and recover it when necessary. Another class, called `ForecastDb`, will make use of it.