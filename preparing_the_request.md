# Preparing the request

As we need to know which item we are going to show in the detail activity, logic tells we’ll need to send the `id` of the forecast to the detail. So the domain model needs a new `id` property:
```kotlin
data class Forecast(val id: Long, val date: Long, val description: String,
    val high: Int, val low: Int, val iconUrl: String)
```

The `ForecastProvider` also needs a new function, which returns the requested forecast by id. The `DetailActivity` will need it to recover the forecast based on the id it will receive. As all the requests always iterate over the sources and return the first non-null result, we can extract that behaviour to another function:
```kotlin
private fun <T : Any> requestToSources(f: (ForecastDataSource) -> T?): T
        = sources.firstResult { f(it) }
```

The function is generified using a non-nullable type. It will receive a function which uses a `ForecastDataSource` to return an nullable object of the generic type, and will finally return a nonnullable object. We can rewrite the previous request and write the new one this way:

```kotlin
fun requestByZipCode(zipCode: Long, days: Int): ForecastList = requestToSources {
    val res = it.requestForecastByZipCode(zipCode, todayTimeSpan())
    if (res != null && res.size() >= days) res else null
}

fun requestForecast(id: Long): Forecast = requestToSources {
    it.requestDayForecast(id)
}
```

Now the data sources need to implement the new function:
```kotlin
fun requestDayForecast(id: Long): Forecast?
```

The `ForecastDb` will always have the required value already cached from previous requests, so we can get it from there this way:
```kotlin
override fun requestDayForecast(id: Long): Forecast? = forecastDbHelper.use {
    val forecast = select(DayForecastTable.NAME).byId(id).
            parseOpt { DayForecast(HashMap(it)) }
    if (forecast != null) dataMapper.convertDayToDomain(forecast) else null
}
```
The `select` query is very similar to the previous one. I created another utility function called `byId`, because a request by `id` is so common that a function like that simplifies the process and is easier to read. The implementation of the function is quite simple:
```kotlin
fun SelectQueryBuilder.byId(id: Long): SelectQueryBuilder
        = whereSimple("_id = ?", id.toString())
```

It just makes use of the `whereSimple` function and implements the search over the `_id field`. Thisfunction is quite generic, but as you can see, you could create as many extension functions as you
need based on the structure of your database, and hugely simplify the readability of your code. The `DbDataMapper` has some slight changes not worth mentioning. You can check them in the repository.


On the other hand, the `ForecastServer` will never be used, because the info will be always cached in the database. We could implement it to defend our code from uncommon edge cases, but we’re not doing it in this case, so it will just throw an exception if it’s called:
```kotlin
override fun requestDayForecast(id: Long): Forecast?
        = throw UnsupportedOperationException()
```

>### `try` and `throw` are expressions
>>In Kotlin, almost everything is an expression, which means it returns a value. This is really important for functional programming, and particularly useful when dealing with edge cases with `try-catch` or when throwing exceptions. For instance, the example above shows how we can assign an exception to the result even if they are not of the same type, instead of having to create a full block of code. This is very useful too when we want to throw an exception in one of `when` branches:
>>```
>>  val x = when(y){ 
                in 0..10 -> 1
                in 11..20 -> 2
                else -> throw Exception("Invalid")
    }
>>```
>>The same happens with `try-catch`, we can assign a value depending on the result of the try:
>>```kotlin
>>val x = try { doSomething() } catch { null }
>>```

The last thing we need to be able to perform the request from the new activity is to create a command. The code is really simple:
```kotlin
class RequestDayForecastCommand(
        val id: Long,
        val forecastProvider: ForecastProvider = ForecastProvider()) :
        Command<Forecast> {
    override fun execute() = forecastProvider.requestForecast(id)
}
```
The request returns a `Forecast` result that will be used by the activity to draw its UI.

