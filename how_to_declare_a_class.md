# How to declare a class

Nếu bạn muốn khai báo một class, bạn chỉ cần sử dụng keyword `class`:
```kotlin
class MainActivity{

}
```

Các class có constructor mặc định duy nhất. Chúng ta sẽ thấy rằng chúng ta có thể tạo các contructors mới cho các trường hợp ngoại lệ, như giữ trong đầu rằng hầu hết các trường hợp chỉ cần một single constructor. Parameters được viết ngay sau tên. Có thể bỏ dấu ngoặc đơn nếu class không có content:

```kotlin
class Person(name: String, surname: String)
```
Vậy body của constructor nằm ở đâu? bạn có thể khai báo init block:

```kotlin
class Person(name: String, surname: String) {
    init{
        ...
    }
}```