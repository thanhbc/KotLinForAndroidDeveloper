# When expression

`When` expressions are similar to `switch/case` in Java, but far more powerful. This expression will try to match its argument against all possible branches in order until it finds one that is satisfied. It will then apply the right side of the expression. The difference with a `switch/case` in Java is that the argument can be literally anything, and the conditions for the branches too.

For the default option, we can add an `else` branch that will be executed if none of the previous conditions is satisfied. The code executed when a condition is satisfied can be a block too:
```kotlin
when (x){
    1 -> print("x == 1") 
    2 -> print("x == 2") 
    else -> {
        print("I'm a block")
        print("x is neither 1 nor 2")
    }
}
```

As it’s an expression, it can return a result too. Take into consideration that when used as an expression, it must cover all the possible cases or implement the `else` branch. It won’t compile otherwise:
```kotlin
val result = when (x) {
    0, 1 -> "binary"
    else -> "error"
}
```

As you can see, the condition can be a set of values separated with commas. But it can be many more things. We could, for instance, check the type of the argument and take decisions based on this:
```kotlin
when(view) {
    is TextView -> view.setText("I'm a TextView")
    is EditText -> toast("EditText value: ${view.getText()}")
    is ViewGroup -> toast("Number of children: ${view.getChildCount()} ")
    else -> view.visibility = View.GONE
}
```

The argument is automatically casted in the right part of the condition, so you don’t need to do any explicit casting.

It’s possible to check whether the argument is inside a range (I’ll explain ranges later in this chapter), or even inside a collection:
```kotlin
val cost = when(x) {
    in 1..10 -> "cheap"
    in 10..100 -> "regular"
    in 100..1000 -> "expensive"
    in specialValues -> "special value!"
    else -> "not rated"
}
```

Or you could even get rid of the argument and do any crazy check you may need. It could easily substitute an `if / else` chain:
```kotlin
valres=when{
    x in 1..10 -> "cheap"
    s.contains("hello") -> "it's a welcome!"
    v is ViewGroup -> "child count: ${v.getChildCount()}"
    else -> ""
}
```