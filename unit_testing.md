# Unit testing

I’m not entering into discussions about what unit testing is. There are many definitions out there with some slight differences. A general idea could be that unit tests are the tests that validate an individual unit of source code. What a ‘unit’ includes is left to the reader. In our case, I’m just going to define a unit test as a test that doesn’t need a device to be run. The IDE will be able to run the tests and show a result identifying which tests succeeded and which ones failed.

Unit testing is usually done using `JUnit` library. So let’s add the dependency to the `build.gradle`. As this dependency is only used when running tests, we can use `testCompile` instead of compile. This way, the library is left out of regular compilations, reducing the size of the APK:
```kotlin
dependencies {
    ...
    testCompile 'junit:junit:4.12'
}
```

Now sync gradle to get the library included into your project. In some versions of Android Studio, you may need to choose which kind of tests you want to run. Go to the ‘Build Variants’ tab (you probably have it in the left side of the IDE) and click on ‘Test Artifact’ dropdown. You should choose ‘Unit Tests’ there. New Android Studio versions will be able to run enable both kind of tests at the same time, so this step won’t be required.

Another thing you need is to create a new folder. Below src, you already probably have `androidTest` and main. Create another one called test, and a folder called `java` below. So now you should have a `src/test/java` folder coloured in green. This is a good indication that the IDE detected that we are in ‘Unit Test’ mode and that this folder will contain test files.

Let’s write a very simple test to see everything works properly. Create a new Kotlin class called `SimpleTest` using the proper package (in my case `com.antonioleiva.weatherapp`, but you need to use the main package of your app). Once you’ve created the new file, write this simple test:
```kotlin
import org.junit.Test
import kotlin.test.assertTrue
class SimpleTest {
    @Test fun unitTestingWorks() {
        assertTrue(true)
    }
}
```

Use the `@Test` annotation to identify the function as a test. Be sure to use `org.unit.Test`. Then add a simple assertion. It will just check that true is `true`, which should obviously succeed. This test will just check that everything is configured properly.

To execute the tests, just right click over the new `java` folder you created below test, and choose ‘Run All Tests’. When compilation finishes, it will run the test and you’ll see a summary showing the result. You should see that your test passed.

Now it’s time to create some real tests. Everything that deals with Android framework will probably need an instrumentation test or more complex libraries such as [Robolectric²⁹]. Because of that, in these examples I’ll be testing things that don’t use anything from the framework. For instance, I’ll test the extension function that creates a date `String` from a `Long`.

Create a new file called `ExtensionTests`, and add this tests:
```kotlin
class ExtensionsTest {

    @Test fun testLongToDateString() {
        assertEquals("Oct 19, 2015", 1445275635000L.toDateString())
    }

    @Test fun testDateStringFullFormat() {
        assertEquals("Monday, October 19, 2015",
            1445275635000L.toDateString(DateFormat.FULL))
    }
}
```

These tests check that a `Long` instance is properly converted to a String. The first one tests the default behaviour (which uses `DateFormat.MEDIUM`), while the second one specifies a different format. Run the tests and see that all of them pass. I also recommend you to change something and see how it crashes.


If you’re used to test your apps in Java, you’ll see there’s not much difference here. I’ve covered a very simple example, but from here you can create more complex tests to validate other parts of the App. For instance, we could do some tests about `ForecastProvider`. We can use `Mockito` library to mock some other classes and be able to test the provider independently:
```kotlin
dependencies {
    ...
    testCompile "junit:junit:4.12"
    testCompile "org.mockito:mockito-core:1.10.19"
}
```

Now create a `ForecastProviderTest`. We are going to test that a `ForecastProvider` with a `DataSource` that returns something will get a result that is not null. So first we need to mock a `ForecastDataSource`:
```kotlin
val ds = mock(ForecastDataSource::class.java)
`when`(ds.requestDayForecast(0)).then {
    Forecast(0, 0, "desc", 20, 0, "url")
}
```

As you see, we need backquotes for `when` function. This is because `when` is a reserved word in Kotlin, so we need to escape it if we find some Java code that uses it. Now we create a provider with this data source, and check that the result of the call to that method is not null:
```kotlin
val provider = ForecastProvider(listOf(ds))
assertNotNull(provider.requestForecast(0))
```

This is the complete test function:

```kotlin
@Test fun testDataSourceReturnsValue() {
    val ds = mock(ForecastDataSource::class.java)
    `when`(ds.requestDayForecast(0)).then {
           Forecast(0, 0, "desc", 20, 0, "url")
    }

    val provider = ForecastProvider(listOf(ds))
    assertNotNull(provider.requestForecast(0))
}
```

If you run this, you’ll see that it crashes. Thanks to this test, we detect we have something wrong in our code. The test is failing because `ForecastProvider` is initialising `SOURCES` inside its companion object before it’s used. We can add some sources to the `ForecastProvider` through the constructor, and this static list would never be used, so it should be lazy loaded:
```kotlin
companion object {
    val DAY_IN_MILLIS = 1000 * 60 * 60 * 24
    val SOURCES by lazy { listOf(ForecastDb(), ForecastServer()) }
}
```

If you now run again, you’ll see it’s now passing all the tests.

We can also test, for instance, that when a source returns null, it will iterate over the next source to get a result:
```kotlin
@Test fun emptyDatabaseReturnsServerValue() {
    val db = mock(ForecastDataSource::class.java)
    val server = mock(ForecastDataSource::class.java)
    `when`(server.requestForecastByZipCode(
            any(Long::class.java), any(Long::class.java)))
            .then {
                ForecastList(0, "city", "country", listOf())
    val provider = ForecastProvider(listOf(db, server))
    assertNotNull(provider.requestByZipCode(0, 0))
}
```

As you see, the simple dependency inversion we solved by using default values for arguments is enough to let us implement some simple unit tests. There are many more things we could test about this provider, but this example is enough to show that we are able to use the basic unit testing tools.

[Robolectric²⁹]:http://robolectric.org