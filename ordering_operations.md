# Ordering operations


### reverse
Returns a list with elements in reversed order.
```kotlin
val unsortedList = listOf(3, 2, 7, 5)
assertEquals(listOf(5, 7, 2, 3), unsortedList.reverse())
```

### sort
Returns a sorted list of all elements.
```kotlin
assertEquals(listOf(2, 3, 5, 7), unsortedList.sort())
```

### sortBy
Returns a list of all elements, sorted by the specified comparator.
```kotlin
assertEquals(listOf(3, 7, 2, 5), unsortedList.sortBy { it % 3 })
```

### sortDescending
Returns a sorted list of all elements, in descending order.
```kotlin
assertEquals(listOf(7, 5, 3, 2), unsortedList.sortDescending())
```

### sortDescendingBy
Returns a sorted list of all elements, in descending order by the results of the specified order function.
```kotlin
assertEquals(listOf(2, 5, 7, 3), unsortedList.sortDescendingBy { it % 3 })
```









