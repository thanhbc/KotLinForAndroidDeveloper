# Creating a new project

Nếu bạn đã sử dụng Android Studio và Gradle, chương này sẽ đơn giản thôi. Tôi không muốn đưa bất cứ chi tiết nào cũng như hình ảnh, vì giao diện thường hay thay đổi và những hình ảnh đó cũng chẳng làm cái méo gì.

Ứng dụng của chúng ta sẽ là một app weather đơn giản tương tự như ứng dụng được dùng trong khoá [Google’s Beginners Course trên Udacity¹⁰](https://www.udacity.com/course/android-development-for-beginners--ud837). Chúng ta sẽ tập trung vào những cái khác biệt, tuy nhiên ý tưởng thì vẫn tương tự, bởi vì nó sẽ bao gồm nhiều thứ bạn sẽ tìm thấy trong một app căn bản. Nếu trình Android của bạn còn cùi. Mình recommend khoá này vì nó dễ theo dõi.