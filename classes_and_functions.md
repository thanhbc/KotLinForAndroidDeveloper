# Classes and functions

Class trong Kotlin follow theo một cấu trúc đơn giản. Tuy nhiên, có một số khác biệt với Java mà bạn sẽ muôn biết trước khi chúng ta tiếp tục. Bạn có thể dùng [try.kotlinlang.org¹²](http://try.kotlinlang.org) để kiểm tra và xem một số ví dụ đơn giản khác mà không cần tạo project.