# Sealed classes

Sealed classes are used to represent restricted class hierarchies, which means that the number of classes that extend a sealed class is fixed. It’s similar to an Enum in the sense that you can know beforehand the options you have when looking at the specific type of an object of the parent sealed class. The difference is that enum instances are unique, while sealed classes can have multiple instances which can contain different states.

We could implement, for instance, something similar to the `Option` class in Scala: a type that prevents the use of `null` by returning a `Some` class when the object contains a value or the `None` instance when it’s empty:
```kotlin
sealed class Option<out T> {
    class Some<out T> : Option<T>()
    object None : Option<Nothing>()
}
```

The good thing about sealed classes is that when they are used in a `when` expression, we can check all the options and won’t need to add the `else` clause.
```kotlin
val result = when (option) {
    is Option.Some<*> -> "Contains a value"
    is Option.None -> "Empty"
}
```