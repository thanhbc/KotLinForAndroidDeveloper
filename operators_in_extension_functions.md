# Operators in extension functions

We don’t need to stick to our own classes, but we could even extend existing classes using extension functions to provide new operations to third party libraries. For instance, we could access to `ViewGroup` views the same way we do with lists:

```kotlin
operator fun ViewGroup.get(position: Int): View = getChildAt(position)
```
Now it’s really simple to get a view from a `ViewGroup` by its position:
```kotlin
val container: ViewGroup = find(R.id.container)
val view = container[2]
```