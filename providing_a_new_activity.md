# Providing a new activity

We are now prepared to create the `DetailActivity`. Our detail activity will receive a couple of parameters from the main one: the forecast id and the name of the city. The first one will be used to request the data from the database, and the name of the city will fill the toolbar. So we first need a couple of names to identify the parameters in the bundle:
```kotlin
public class DetailActivity : AppCompatActivity() {
    companion object {
        val ID = "DetailActivity:id"
        val CITY_NAME = "DetailActivity:cityName"
    }
    ...
}
```

In `onCreate` function, the first step is to set the content view. The UI will be really simple, but more than enough for this app example:
```kotlin
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:paddingBottom="@dimen/activity_vertical_margin"
    android:paddingLeft="@dimen/activity_horizontal_margin"
    android:paddingRight="@dimen/activity_horizontal_margin"
    android:paddingTop="@dimen/activity_vertical_margin">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        android:gravity="center_vertical"
        tools:ignore="UseCompoundDrawables">

        <ImageView
            android:id="@+id/icon"
            android:layout_width="64dp"
            android:layout_height="64dp"
            tools:src="@mipmap/ic_launcher"
            tools:ignore="ContentDescription"/>

        <TextView
            android:id="@+id/weatherDescription"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="@dimen/spacing_xlarge"
            android:textAppearance="@style/TextAppearance.AppCompat.Display1"
            tools:text="Few clouds"/>
    </LinearLayout>
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content">
        <TextView
            android:id="@+id/maxTemperature"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:layout_margin="@dimen/spacing_xlarge"
            android:gravity="center_horizontal"
            android:textAppearance="@style/TextAppearance.AppCompat.Display3"
            tools:text="30"/>
        <TextView
            android:id="@+id/minTemperature"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:layout_margin="@dimen/spacing_xlarge"
            android:gravity="center_horizontal"
            android:textAppearance="@style/TextAppearance.AppCompat.Display3"
            tools:text="10"/>
    </LinearLayout>
</LinearLayout>
```

Then assign it from `onCreate` code. Use the city name to fill the toolbar title. The methods for `intent` and `title` are automatically mapped to a property:
```kotlin
setContentView(R.layout.activity_detail)
title = intent.getStringExtra(CITY_NAME)
```
The other part in `onCreate` implements the call to the command. It’s very similar to the call we previously did:
```kotlin
async {
       val result = RequestDayForecastCommand(intent.getLongExtra(ID, -1)).execute()
       uiThread { bindForecast(result) }
}
```

When the result is recovered from the database, the `bindForecast` function is called in the UI thread. I’m using Kotlin Android Extensions plugin again in this activity, to get the properties from the XML without using `findViewById`:
```kotlin
import kotlinx.android.synthetic.activity_detail.*
...

private fun bindForecast(forecast: Forecast) = with(forecast) {
       Picasso.with(ctx).load(iconUrl).into(icon)
       supportActionBar.subtitle = date.toDateString(DateFormat.FULL)
       weatherDescription.text = description
       bindWeather(high to maxTemperature, low to minTemperature)
}
```
There are some interesting things here. For instance, I’m creating another extension function able to convert a `Long` object into a visual date string. Remember we were using it in the adapter too, so it’s a good moment to extract it into a function:
```kotlin
fun Long.toDateString(dateFormat: Int = DateFormat.MEDIUM): String {
    val df = DateFormat.getDateInstance(dateFormat, Locale.getDefault())
    return df.format(this)
}
```

It will get a date format (or use the default `DateFormat.MEDIUM`) and convert the `Long` into a `String` that is understandable by the user.

Another interesting function is `bindWeather`. It will get a `vararg` of pairs of `Int` and `TextView`, and assign a text and a text color to the `TextViews` based on the temperature.

```kotlin
private fun bindWeather(vararg views: Pair<Int, TextView>) = views.forEach {
    it.second.text = "${it.first.toString()}��"
    it.second.textColor = color(when (it.first) {
        in -50..0 -> android.R.color.holo_red_dark
        in 0..15 -> android.R.color.holo_orange_dark
        else -> android.R.color.holo_green_dark
    })
}
```

For each pair, it assigns the text that will show the temperature and a color based on the value of the temperature: red for low temperatures, orange for mild ones and green for the rest. The values are taken quite randomly, but it’s a good representation of what we can do with a `when` expression, how clean and short the code becomes.

`color` is an extension function I miss from Anko, which simplifies the way to get a color from resources, similar to the `dimen` one we’ve used in some other places. At the time of writing this lines, current support library relies on the class `ContextCompat` to get a color in a compatible way for all Android versions:

```kotlin
public fun Context.color(res: Int): Int = ContextCompat.getColor(this, res)
```

I was missing a property representation for `textColor`. The thing is `TextView` lacks `getTextColor()` method, so it’s not automatically parsed. A definition could be this one:
```kotlin
var TextView.textColor: Int
   get() = currentTextColor
   set(v) = setTextColor(v)
```

There is an implementation in another Anko package (it returns an exception in get, it could be an alternative), but it’s the one related to the creation of views using a DSL. If you are implementing your views using regular XML, I recommend not to add this library only to use one or two functions. That part of the library is huge and you will waste a good part of method count if you don’t use Proguard.

The `AndroidManifest` also needs to be aware that a new activity exists:
```kotlin
<activity
    android:name=".ui.activities.DetailActivity"
    android:parentActivityName=".ui.activities.MainActivity" >
    <meta-data
        android:name="android.support.PARENT_ACTIVITY"
        android:value="com.antonioleiva.weatherapp.ui.activities.MainActivity" />
</activity>
```