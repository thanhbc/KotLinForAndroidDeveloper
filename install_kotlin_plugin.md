# Install Kotlin plugin
Từ Intellij 15 plugin được cài đặt mặc định, tuy nhiên đối với Android Studio thì vẫn chưa được cài đặt. Vì vậy bạn cần phải đến section plugins trong Android Studio Preferences, và cài đặt Kotlin plugin. Sử dụng search tool nếu bạn không tìm thấy.

Bây giờ thì môi trường của chúng ta đã sẵn sàng hiểu được ngôn ngữ, biên dịch và thực thi tương tự như chúng ta sử dụng Java.
