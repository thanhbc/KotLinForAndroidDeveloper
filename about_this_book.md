# About this book
Trong quyển sách này, tôi sẽ tạo một ứng dụng android từ đầu sử dụng Kotlin là ngôn ngữ chính. Ý tưởng là sẽ học ngôn ngữ này qua ví dụ, thay vì theo cấu trúc của của một quyển sách tham khảo.

Tôi sẽ dừng lại để giải thích về những khái niệm và ý tưởng của Kotlin, so sánh nó với Java 7. Bằng cách này, bạn sẽ có thể thấy được sự khác biệt và phần nào của ngôn ngữ sẽ giúp bạn tăng tốc độ làm việc.

Cuốn sách này không được xem như một tài liệu tham khảo, nhưng là một công cụ cho các nhà phát triển Android để học Kotlin và có thể tiếp tục với những dự án của họ. Tôi sẽ giải quyết nhiều vấn đề điển hình mà chúng ta gặp phải hằng ngày bằng cách sử dụng ngôn ngữ và một số công cụ cũng như thư viện hữu ích. Tuy nhiên, nội dung sẽ bao quát hầu hết với ngôn ngữ Kotlin, vì vậy khi đọc hết quyển sách bạn sẽ có kiến thức sâu sắc về ngôn ngữ.

Quyển sách này thiên về thực hành, vì vậy khuyến khích mọi người theo dõi ví dụ và code với máy tính trước mặt và viết lại tất cả các ví dụ. Bạn cũng có thể đọc trước để có khái niệm sau đó thực hành.

Như bạn đã đọc ở các trang trước (và cả trên trang mà bạn tải về), đây là một ấn phẩm. Nghĩa là quyển sách đang trong quá trình viết và cám ơn những người đọc và bình luận. Mặc dù hiện tại đã hoàn thành, Tôi sẽ review nhiều lần để cập nhật với các phiên bản mới nhất của Kotlin. Vì vậy, tôi cần sự nhận xét của bạn về việc bạn nghĩ gì về quyển sách này, hoặc điều gì cần được cải thiện.
Tôi muốn quyển sách này trở thành công cụ tuyệt vời cho các nhà phát triển Android, bất kỳ sự giúp đỡ hoặc ý tưởng nào đều được hoan nghênh.

Cám ơn vì trở thành một phần của dự án thú vị này.