# For loops

Though you won’t probably use them too much if you make use of functional operators in collections, `for` loops can be useful in some situations, so they are still available. It works with anything that provides an iterator:
```kotlin
for (item in collection) {
    print(item)
}
```
If we want to get a more typical iteration over indices, we can also do it using ranges (there are usually smarter solutions anyway):
```kotlin
for (index in 0..viewGroup.getChildCount() - 1) {
    val view = viewGroup.getChildAt(index)
    view.visibility = View.VISIBLE
}
```

When iterating over an array or a list, a set of indices can be requested to the object, so the previous artifact is not necessary:
```kotlin
for (i in array.indices)
    print(array[i])
```