# Operator overloading

Kotlin has a fixed number of symbolic operators we can easily use on any class. The way is to create a function with a reserved name that will be mapped to the symbol. Overloading these operators will increment code readability and simplicity.

In order to alert the compiler we want to overload an operator, functions must be annotated with the `operator` modifier