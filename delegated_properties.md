# Delegated Properties

There are some kind of common behaviours we may need in a property that would be interesting to be reused, such as lazy values or observable properties. Instead of having to declare the same code over and over again, Kotlin provides a way to delegate the code a property needs to another class. This is know as a **delegated property**.

When we use `get` or `set` from a property, the `getValue` and `setValue` of the delegated property are called.

The structure of a property delegate is:

```kotlin
class Delegate<T> : ReadWriteProperty<Any?, T> {
    fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return ...
    }

    fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        ...
    }
}
```

The T is the type of the property that is delegating its behaviour. The `getValue` function receives a reference to the class and the metadata of the property. The `setValue` function also receives the value that is being assigned. If the property is immutable (val), it will only require the `getValue` function.

This is how the property delegate is assigned:

```kotlin
class Example {
    var p: String by Delegate()
}
```

It uses **_by_** reserved word to specify the delegation.