# Exceptions

In Kotlin, all exceptions implement Throwable, have a message and are unchecked. This means we are not obliged to use `try/catch` on any of them. That’s not the case in Java, where methods that throw IOException, for instance, need to be surrounded by a `try/catch` block. Years of dealing with them have shown that checked exceptions were not a good idea. People like [Bruce Eckel³¹], [Rod Waldhoff³²] or [Anders Hejlsberg³³] can give you a better perspective about it.


The way to throw an exception is very similar to Java:

```kotlin
throw MyException("Exception message")
```

And `try` expression is identical too:
```kotlin
try{
    // some code
}
catch (e: SomeException) {
    // handler
}
finally {
    // optional finally block
}
```

Both `throw` and `try` are expressions in Kotlin, which means they can be assigned to a variable. This is really useful when dealing with edge cases:

```kotlin
val s = when(x){
    is Int -> "Int instance"
    is String -> "String instance"
    else -> throw UnsupportedOperationException("Not valid type")
}
```
or

```kotlin
val s = try { x as String } catch(e: ClassCastException) { null }
```
[Bruce Eckel³¹]:http://www.mindview.net/Etc/Discussions/CheckedExceptions
[Rod Waldhoff³²]:http://radio-weblogs.com/0122027/stories/2003/04/01/JavasCheckedExceptionsWereAMistake.html
[Anders Hejlsberg³³]:http://www.artima.com/intv/handcuffs.html