# Generic preference delegate

Now that we are generics experts, why not extending `LongPreference` to be used with any type that Shared Preferences support? Let’s create a new `Preference` delegate:
```kotlin
class Preference<T>(val context: Context, val name: String, val default: T)
    : ReadWriteProperty<Any?, T> {

    val prefs by lazy {
        context.getSharedPreferences("default", Context.MODE_PRIVATE)
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return findPreference(name, default)
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        putPreference(name, value)
    }
    ...
}
```

This preference is very similar to what we had before. We just substituted the `Long` references with a generic type T, and called to a couple of functions that will do the hard work. These functions are very simple, though a bit repetitive. They will check the type and use the specific method from preferences. For instance, the `findPreference` function looks like this:
```kotlin
private fun <T> findPreference(name: String, default: T): T = with(prefs) {
        val res: Any = when (default) {
        is Long -> getLong(name, default)
        is String -> getString(name, default)
        is Int -> getInt(name, default)
        is Boolean -> getBoolean(name, default)
        is Float -> getFloat(name, default)
        else -> throw IllegalArgumentException(
            "This type can be saved into Preferences")
    }

    res as T
}
```

And basically the same for `putPreference` function, but using the preferences editor and saving the result of `when` at the end, by calling `apply()`:

```kotlin
private fun <U> putPreference(name: String, value: U) = with(prefs.edit()) {
    when (value) {
        is Long -> putLong(name, value)
        is String -> putString(name, value)
        is Int -> putInt(name, value)
        is Boolean -> putBoolean(name, value)
        is Float -> putFloat(name, value)
        else -> throw IllegalArgumentException("This type can be saved into Pref\
erences")
    }.apply()
}
```

Now update `DelegatesExt` object and you’re done:
```kotlin
object DelegatesExt {
    ...
    fun preference<T : Any>(context: Context, name: String, default: T)
        = Preference(context, name, default)
}
```

After this chapter, the user can now access the settings screen and modify the zip code. That way, when they return to the main screen, the forecast will automatically be refreshed with the new information. Check the rest of small tweaks at the repository.