# Convert MainActivity to Kotlin code

Một chức năng thú vị của plugin Kotlin là khả năng convert từ Java qua Kotlin code. Giống như những xử lý tự động khác, nó sẽ không hoàn hoản, tuy nhiên nó sẽ giúp ích nhiều trong suốt ngày đầu tiên cho đến khi bạn bắt đầu sử dụng ngôn ngữ Kotlin.

Chúng ta sử dụng nó trong lớp `MainActivity.java`. Mở file và chọn `Code -> Convert Java File to Kotlin File`. Xem sự khác biệt trước khi bạn bắt đầu quen với ngôn ngữ này.