# Delegation

The [delegation pattern²⁶] is a really useful pattern that can be used to extract responsibilities from a class. The delegation pattern is supported natively by Kotlin, so it prevents from the need of calling the delegate. The delegator just needs to specify which instance implements the interface.

In our previous example, we can specify how the animal flies through the constructor, instead of implementing it. For instance, a flying animal that uses wings to fly can be specified this way:
```kotlin
interface CanFly {
    fun fly()
}

class Bird(f: CanFly) : CanFly by f
```

We can indicate that a bird can fly by using the interface, but the way the bird uses to fly is defined through a delegate that is defined in the constructor, so we can have different birds with different flying methods. The way an animal with wings flies is defined in another class:
```kotlin
class AnimalWithWings : CanFly {
    val wings: Wings = Wings()
    override fun fly() = wings.move()
}
```
An animal with wings moves its wings to be able to fly. So now we can create a bird that flies using wings:
```kotlin
val birdWithWings = Bird(AnimalWithWings())
birdWithWings.fly()
```

But now wings can be used with another animals that are not birds. If we assume that bats always use wings, we could instantiate the object directly where we specify the delegation:
```kotlin
class Bat : CanFly by AnimalWithWings()
...
val bat = Bat()
bat.fly()
```

[delegation pattern²⁶]:https://en.wikipedia.org/wiki/Delegation_pattern