# Writing and requesting data

The `SqliteOpenHelper` is just the tool, the channel between object oriented and SQL worlds. We’ll use it in a new class, to request data already saved in the database, and to save fresh data. The definition of the class will be using a `ForecastDbHelper` and a `DataMapper` that will convert classes from database to domain models. I’m still using default values as an easy way of dependency injection:
```kotlin
class ForecastDb(
    val forecastDbHelper: ForecastDbHelper = ForecastDbHelper.instance,
    val dataMapper: DbDataMapper = DbDataMapper()) {
    ...
}
```

Both functions will make use of the `use()` function we saw in the previous chapter. The value that the lambda returns will be used as the result of our function. So let’s define a function that requests a forecast based on a `zip code` and a `date`:
```kotlin
fun requestForecastByZipCode(zipCode: Long, date: Long) = forecastDbHelper.use {
    ...
}
```

Not much to explain here: we return the result of the `use` function as the result of our function.


### Requesting a forecast

The first request that needs to be done is the daily forecast, because we need the list to create the city object. Anko provides a simple request builder, so let’s take advantage of it:

```kotlin
val dailyRequest = "${DayForecastTable.CITY_ID} = ? " +
    "AND ${DayForecastTable.DATE} >= ?"

val dailyForecast = select(DayForecastTable.NAME)
        .whereSimple(dailyRequest, zipCode.toString(), date.toString())
        .parseList { DayForecast(HashMap(it)) }
```

The first line, `dailyRequest`, is the `where` part of the query. This is the first parameter the `whereSimple` function needs, and it’s very similar to what we’d do in a regular use of the helper. There is another function called simply where, which takes some tags and values and match them. I don’t find it very useful because I think it adds more boilerplate, though it has the advantage of parsing the values to the Strings we need. This is how it would look with it:
```kotlin
val dailyRequest = "${DayForecastTable.CITY_ID} = {id}" + "AND ${DayForecastTable.DATE} >= {date}"

val dailyForecast = select(DayForecastTable.NAME)
        .where(dailyRequest, "id" to zipCode, "date" to date)
        .parseList { DayForecast(HashMap(it)) }
```

You can choose your preferred one. The `select` function is simple, it just asks for the name of the table. The `parse` methods are where magic happens. In this case we are using the function parseList, which assumes we are requesting a list of items. It uses a `RowParser` or `MapRowParser` to convert the cursor into a list of object. The difference between both is that the `RowParser` relies on the order of the columns, while the `MapRowParser` uses the name of the column as the key of the map.

These two overloads conflict between them, so we can’t directly use the simplification that prevents from the need of creating an object explicitly. But nothing that can’t be solved with an extension function. I’m creating a function that receives a lambda and returns a `MapRowParser`. The parser will use that lambda to create the object:
```kotlin
fun <T : Any> SelectQueryBuilder.parseList(
    parser: (Map<String, Any>) -> T): List<T> =
        parseList(object : MapRowParser<T> {
            override fun parseRow(columns: Map<String, Any>): T = parser(columns)
})
```

This function helps simplify the `parseList` request to:
```kotlin
parseList { DayForecast(HashMap(it)) }
```

The immutable map that the parser receives is converted into a mutable map (we need it to be mutable in our database model) by using the corresponding constructor from the `HashMap`. This `HashMap` is used by the constructor of `DayForecast`.

So, to understand what is happening behind the scenes, the request returns a `Cursor`. `parseList` iterates over it and gets the rows from the `Cursor` until it reaches the last one. For each row, it creates a map with the columns as keys and assigns the value to the corresponding key. The map is then returned to the parser.

If there are no results for the request, `parseList` returns an empty list.

The next step is to request the city, in a similar way:
```kotlin
val city = select(CityForecastTable.NAME)
        .whereSimple("${CityForecastTable.ID} = ?", zipCode.toString())
        .parseOpt { CityForecast(HashMap(it), dailyForecast) }
```

The difference here: we are using `parseOpt` instead. This function returns a nullable object. The result can be null or a single object, depending on whether the request finds something in the database or
not. There is another function called parseSingle, which does essentially the same, but returns a non-nullable object. So if it doesn’t find a row in the database, it throws an exception. In our case, first time we query a city it won’t be there, so using `parseOpt` is safer. I also created a handy function to prevent the need of an object creation:

```kotlin
public fun <T : Any> SelectQueryBuilder.parseOpt(
    parser: (Map<String, Any>) -> T): T? =
        parseOpt(object : MapRowParser<T> {
            override fun parseRow(columns: Map<String, Any>): T = parser(columns)
        })
```

Finally, if the returned city is not null, we convert it to a domain object and return it, using the `dataMapper`. Otherwise, we just return null. As you may remember, last line inside a lambda represents what the lambda returns. So it will return an object from the type `CityForecast?`:
```kotlin
if (city != null) dataMapper.convertToDomain(city) else null
```
`DataMapper` function is easy:
```kotlin
fun convertToDomain(forecast: CityForecast) = with(forecast) {
    val daily = dailyForecast.map { convertDayToDomain(it) }
    ForecastList(_id, city, country, daily)
}

private fun convertDayToDomain(dayForecast: DayForecast) = with(dayForecast) {
    Forecast(date, description, high, low, iconUrl)
}
```

So this is how the complete function looks like:

```kotlin
fun requestForecastByZipCode(zipCode: Long, date: Long) = forecastDbHelper.use {

    val dailyRequest = "${DayForecastTable.CITY_ID} = ? AND " +
        "${DayForecastTable.DATE} >= ?"
    val dailyForecast = select(DayForecastTable.NAME)
            .whereSimple(dailyRequest, zipCode.toString(), date.toString())
            .parseList { DayForecast(HashMap(it)) }

    val city = select(CityForecastTable.NAME)
            .whereSimple("${CityForecastTable.ID} = ?", zipCode.toString())
            .parseOpt { CityForecast(HashMap(it), dailyForecast) }

    if (city != null) dataMapper.convertToDomain(city) else null
}
```

Another interesting functionality from Anko I’m not showing here is that you can make use of a `classParser()` instead of the `MapRowParser` we are using, which uses reflection to fill a class based on the names of the columns. I prefer the other way because we don’t need reflection and have more control over the transformations, but it can be of use for you at some time.


### Saving a forecast

The `saveForecast` function just clears the data from the database so that we save fresh data, converts the domain forecast model to database model, and inserts each daily forecast and the city forecast. The structure is similar to the previous one: it returns the value from the `use` function from the database helper. In this case we don’t need a result, so it’ll return `Unit`.

```kotlin
fun saveForecast(forecast: ForecastList) = forecastDbHelper.use {
    ...
}
```

First, we clear both tables. Anko doesn’t provide any beautiful way to do it, but it doesn’t mean we can’t. So we are creating an extension function for `SQLiteDatabase` that will execute the proper SQL query for us:
```kotlin
fun SQLiteDatabase.clear(tableName: String) {
    execSQL("delete from $tableName")
}
```
The function is applied to both tables:
```kotlin
clear(CityForecastTable.NAME)
clear(DayForecastTable.NAME)
```
Now it’s time to convert the data, and use the result to execute the `insert` queries. At this point you probably know I’m a fan of the `with` function:

```kotlin
with(dataMapper.convertFromDomain(forecast)) {
    ...
}
```

The conversion from the `domain model` is straightforward too:
```kotlin
fun convertFromDomain(forecast: ForecastList) = with(forecast) {
    val daily = dailyForecast.map { convertDayFromDomain(id, it) }
    CityForecast(id, city, country, daily)
}

private fun convertDayFromDomain(cityId: Long, forecast: Forecast) =
    with(forecast) {
        DayForecast(date, description, high, low, iconUrl, cityId)
    }
```
Inside the block, we can use `dailyForecast` and `map` without the need of referring to a variable, just like if we were inside the class. We are using another Anko function for the insertion, which asks for a table name and a `vararg` of `Pair<String, Any>`. The function will convert the `vararg` to the `ContentValues` object the Android SDK needs. So our task consists of transforming the `map` into a `vararg` array. We are creating another extension function for `MutableMap` to do that:
```kotlin
fun <K, V : Any> MutableMap<K, V?>.toVarargArray():
    Array<out Pair<K, V>> =  map({ Pair(it.key, it.value!!) }).toTypedArray()
```

It works over a `MutableMap` with nullable values (this was a condition from the map delegate), and converts it to an `Array`with non-nullable values (`select` function requisite) of pairs. Don’t worry if you don’t understand this function completely, I will be covering nullity really soon.

So, with this new function we can do:
```kotlin
insert(CityForecastTable.NAME, *map.toVarargArray())
```

It inserts a new row in the `CityForecast` table. The ‘*’ used before the result of `toVarargArray` indicates that the array will be decomposed to a `vararg` parameter. This is done automatically in Java, but we need to make it explicit in Kotlin.


And the same for each daily forecast:

```kotlin
dailyForecast.forEach { insert(DayForecastTable.NAME, *it.map.toVarargArray()) }
```
So, with the use of maps, we’ve been able to convert classes to database registers and viceversa in a very simple way. Once we have these extension functions ready, we can use them for other projects, so it’s a really well paid effort.

The complete code of this function:
```kotlin
fun saveForecast(forecast: ForecastList) = forecastDbHelper.use {
    clear(CityForecastTable.NAME)
    clear(DayForecastTable.NAME)

    with(dataMapper.convertFromDomain(forecast)) {
        insert(CityForecastTable.NAME, *map.toVarargArray())
        dailyForecast forEach {
            insert(DayForecastTable.NAME, *it.map.toVarargArray())
        }
    }
}
```
A lot of new code was involved in this chapter, so you can take a look at the repository to review it.