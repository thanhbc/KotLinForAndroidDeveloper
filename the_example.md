# The example
As you can imagine, Kotlin lists have the array-like operations implemented, so we can access to list items the same way we’d do in Java arrays. But it goes beyond: in mutable lists, the item can also be set directly in a very simple way:
```kotlin
val x = myList[2]
myList[2] = 4
```

If you remember, we have a data class called ForecastList, which basically consists of a list with some extra info. It’d be interesting to access its items directly instead of having to request its internal list to get an item. On a totally unrelated note, I’m also going to implement a `size()` function, which will simplify the current adapter a little more:

```kotlin
data class ForecastList(val city: String, val country: String,
                        val dailyForecast: List<Forecast>) {
    operator fun get(position: Int): Forecast = dailyForecast[position]
    fun size(): Int = dailyForecast.size
}
```

It makes our `onBindViewHolder` a bit simpler:

```kotlin
override fun onBindViewHolder(holder: ViewHolder,
        position: Int) {
    with(weekForecast[position]) {
        holder.textView.text = "$date - $description - $high/$low"
    }
}
```

As well as the `getItemCount()` function:

```kotlin
override fun getItemCount(): Int = weekForecast.size()
```