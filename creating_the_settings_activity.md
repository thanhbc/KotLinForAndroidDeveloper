# Creating the settings activity

A new activity will we opened when the settings option is selected in the overflow menu in the toolbar. So first thing we need is a new `SettingsActivity`:
```kotlin
class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar)
        supportActionBar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> { onBackPressed(); true }
        else -> false
    }
}
```

We’ll save the preference when the user gets out of the screen activity, so we’re going to deal with the `Up` action the same way as the `Back` one, by redirecting the action to `onBackPressed`. By now, let’s create the XML layout. A simple `EditText` will be enough for the preference:
```kotlin
<FrameLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <include layout="@layout/toolbar"/>
    <LinearLayout
        android:orientation="vertical"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_marginTop="?attr/actionBarSize"
        android:padding="@dimen/spacing_xlarge">

        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/city_zipcode"/>

        <EditText
            android:id="@+id/cityCode"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:hint="@string/city_zipcode"
            android:inputType="number"/>

    </LinearLayout>
</FrameLayout>
```

And then just declare the activity in the `AndroidManifest.xml`:
```kotlin
<activity
android:name=".ui.activities.SettingsActivity"
android:label="@string/settings"/>
```