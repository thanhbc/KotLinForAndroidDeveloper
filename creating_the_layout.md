# Creating the layout

Màn hình chính sẽ render list dự báo thời tiết sẽ là một `RecyclerView`, do đó nó cần một dependency mới. Chỉnh sửa file `build.gradle`:

```groovy
dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    compile "com.android.support:appcompat-v7:$support_version" 
    compile "com.android.support:recyclerview-v7:$support_version" ...
}
```

Trong `activity_main.xml`:

```xml
<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
             android:layout_width="match_parent"
             android:layout_height="match_parent">
    <android.support.v7.widget.RecyclerView
        android:id="@+id/forecast_list"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</FrameLayout>
```
Trong `MainActivity.kt`, xoá bỏ dòng chúng ta đã thêm vào để kiểm tra mọi thứ hoạt động tốt (lúc này sẽ hiển thị error). Chúng ta tiếp tục sử dụng `findViewById()` lúc này:
```kotlin
val forecastList = findViewById(R.id.forecast_list) as RecyclerView
forecastList.layoutManager = LinearLayoutManager(this)
```

Như bạn có thể thấy, chúng ta định nghĩa mội biến và ép kiểu về `RecyclerView`. Nó khác đôi chút với Java, và chúng ta sẽ xem những khác biệt này trong chương sau. Cần xác định một `LayoutManager`, sử dụng property thay vì gọi setter. Layout này chỉ cần có một list, vì vậy  `LinearLayoutManager` sẽ thực hiện nó.

>Khởi tạo Object (Object instantiation)
>>Khởi tạo object có đôi chút khác biệt với Java. Như bạn có thể thấy, chúng ta bỏ qua từ `new`. Constructor vẫn được gọi, nhưng chúng ta tiết kiệm được 4 ký tự quý giá. `LinearLayoutManager(this)` khởi tạo instance của object.