# Variables

Variables trong Kotlin có thể dễ dàng được định nghĩa như mutable (`var`) hoặc immutable (`val`). Ý tưởng tương tự như việc sử dụng final trong Java. Nhưng **khái niệm bất biến** (immutability) rất quan trọng trong Kotlin (và những ngôn ngữ hiện đại khác).

Một immutable object là một object mà trạng thái của nó không bị thay đổi sau khi khởi tạo. Nếu bạn cần thay đổi version của đối tương, cần phải tạo một object mới. Điều này sẽ giúp việc lập trình có thể dự đoán và tốt hơn. Trong Java, hầu hết object đều là mutable, điều đó nghĩa là bất kỳ cần nào của đoạn code truy cập vào nó có thể chỉnh sửa nó, ảnh hưởng đến toàn bộ phần còn lại của ứng dụng.

Immutable object là thread-safe bằng việc định nghĩa. Với việc nó không thể thay đổi, không có bất kỳ truy cập nào cần phải được định nghĩa, bởi vì tất cả các luồng đều lấy the same object.

Vì vậy cách suy nghĩ về việc coding thay đổi một chút trong Kotlin nếu chúng ta muốn sử dụng tính bất biến. **The key concept: chỉ sử dụng `val` nhiều nhất có thể**. Có một số trường hợp (đặc biệt trong Android, chúng ta không thể truy cập vào constructor của nhiều class) sẽ không khả thi, nhưng nó sẽ sử dụng rất nhiều.

Một điều nữa được đề cập trước đây là thướng ta không cần phải xác định kiểu dữ liệu của object, nó sẽ được suy ra từ giá trị, điều này sẽ giúp code sạch sẽ và chỉnh sửa nhanh hơn. Chúng ta đã có một số ví dụ ở phần trước.

```kotlin
val s = "Example" // A String
val i = 23 // An Int
val actionBar = supportActionBar // An ActionBar in an Activity context
```

Tuy nhiên, một kiểu dữ liệu cần phải được xác định nếu chúng ta muốn sử dựng generic type:
```kotlin
val a: Any = 23
val c: Context = activity
```