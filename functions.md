# Functions
Các hàm - functions (trong Java là methods - phương thức) được khai báo sử dụng keyword **fun**:

```kotlin
fun onCreate(savedInstanceState: Bundle?) {
}
```
Nếu như bạn không xác định giá trị trả về, nó sẽ trả về `Unit`, tương tự như `void` trong Java, tuy nhiên, ở đây là một object thực sự. Bạn dĩ nhiên có thể xác định một kiểu trả về:
```kotlin
fun add(x: Int, y: Int) : Int {
    return x + y
}
```
>Tip: Không cần thiết phải có dấu chấm phẩy `;`
>>Như bạn có thể thấy ở ví dụ trên, tôi không sử dụng dấu chấm phảy ở cuối câu.


>>Tất nhiên bạn vẫn có thể sử dụng nó, dấu `;` không cần thiết và tốt nhất là không dùng nó. Khi bạn dùng như vậy, bạn sẽ cảm thấy tiết kiệm được rất nhiều thời gian.

Tuy nhiên, nếu kết quả có thể được tính khi sử dụng một single expression, bạn có thể bỏ qua luôn dấu `{}`và sử dụng dấu `=`:
```kotlin
fun add(x: Int,y: Int) : Int = x + y
```