# Nested classes

As in Java, we can define classes inside other classes. If it’s a regular class, it won’t we able to access the members of the out class (it would work as an static class in Java):
```kotlin
class Outer {
  private val bar: Int = 1
    class Nested {
        fun foo() = 2
    }
}

val demo = Outer.Nested().foo() // == 2
```

If we want to access to the members of the outer class, we need to declare it as an inner class:
```kotlin
class Outer {
    private val bar: Int = 1
    inner class Inner{
        fun foo() = bar
    }
}

val demo = Outer().Inner().foo() // == 1
```