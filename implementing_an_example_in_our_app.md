# Implementing an example in our App

Interfaces can be used to extract common code from classes which have some similar behaviour. For instance, we can create an interface that deals with the toolbar of the app. Both `MainActivity` and `DetailActivity` will share similar code that deals with the toolbar.

But first, some changes need to be done to start using a toolbar included in the layout instead of the
standard `ActionBar`. The first thing will be extending a `NoActionBar` theme. That way, the toolbar is not included automatically:
```kotlin
<style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar">
    <item name="colorPrimary">#ff212121</item>
    <item name="colorPrimaryDark">@android:color/black</item>
</style>
```
We are using a `light` theme. Next, let’s create a `toolbar` layout that we can include later in some other layouts:
```kotlin
<android.support.v7.widget.Toolbar
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/toolbar"
    android:layout_width="match_parent"
    android:layout_height="?attr/actionBarSize"
    android:background="?attr/colorPrimary"
    app:theme="@style/ThemeOverlay.AppCompat.Dark.ActionBar"
    app:popupTheme="@style/ThemeOverlay.AppCompat.Light"/>
```
The `toolbar` specifies its background, a dark theme for itself and a light theme for the popups it generates (the overflow menu for instance). We get then the same theme we already had: light theme with dark Action Bar.

Next step will be modifying the `MainActivity` layout to include the toolbar:

```kotlin
<FrameLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <android.support.v7.widget.RecyclerView
        android:id="@+id/forecastList"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:clipToPadding="false"
        android:paddingTop="?attr/actionBarSize"/>

    <include layout="@layout/toolbar"/>
</FrameLayout>
```

Now that the toolbar has been added to the layout, we can start using it. We are creating an interface that will let us:

- Change the title
- Specify whether it shows the up navigation action or not
- Animate the toolbar when scrolling
- Assign the same menu to all activities and an event for the actions

So let’s define the `ToolbarManager`:

```kotlin
interface ToolbarManager {
    val toolbar: Toolbar
    ...
}
```

It will need a toolbar property. Interfaces are stateless, so the property can be defined but no value can be assigned. Classes that implement it will need to override the property.

On the other hand, we can implement stateless properties without the need of being overridden. That is, properties that don’t need a backing field. An example would be a property which deals with the toolbar title:
```kotlin
var toolbarTitle: String
    get() = toolbar.title.toString()
    set(value) {
        toolbar.title = value
    }
```

As the property just uses the toolbar, it doesn’t need to save any new state.

We’re now creating a new function that initialises the toolbar, by inflating a menu and setting a listener:
```kotlin
fun initToolbar(){
    toolbar.inflateMenu(R.menu.menu_main)
    toolbar.setOnMenuItemClickListener {
        when (it.itemId) {
            R.id.action_settings -> App.instance.toast("Settings")
            else -> App.instance.toast("Unknown option")
        }
        true
    }
}
```
We can also add a function that enables the navigation icon in the toolbar, sets an arrow icon and a listener that will be fired when the icon is pressed:
```kotlin
fun enableHomeAsUp(up: () -> Unit) {
       toolbar.navigationIcon = createUpDrawable()
       toolbar.setNavigationOnClickListener { up() }
}

private fun createUpDrawable() = with (DrawerArrowDrawable(toolbar.ctx)){
    progress = 1f
    this
}
```

The function receives the listener, creates the up drawable by using the [DrawerArrowDrawable²⁷] on its final state (when the arrow is already showing) and assigns the listener to the toolbar.

Finally, the interface will provide a function that allows the toolbar to be attached to a scroll, and animates the toolbar depending on the direction of the scroll. The toolbar will be hidden while we are scrolling down and displayed again when scrolling up:

```kotlin
fun attachToScroll(recyclerView: RecyclerView) {
    recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            if (dy > 0) toolbar.slideExit() else toolbar.slideEnter()
        }
    })
}
```
We’ll be creating a couple of extension functions which animate the views in and out of the screen. They check if the animation hasn’t been previously performed. That way it prevents the view from being animated every time the scroll varies:
```kotlin
fun View.slideExit() {
    if (translationY == 0f) animate().translationY(-height.toFlat())
}

fun View.slideEnter() {
    if (translationY < 0f) animate().translationY(0f)
}
```

After implementing the toolbar manager, it’s time to use it in the `MainActivity`, which now must implement the interface:
```kotlin
class MainActivity : AppCompatActivity(), ToolbarManager {
    ...
}
```

We first specify the `toolbar` property. We can implement a lazy find so that the toolbar will be already inflated by the first time we use it:
```kotlin
override val toolbar by lazy { find<Toolbar>(R.id.toolbar) }
```

`MainActivity` will just initialise the toolbar, attach to the `RecyclerView` scroll and modify the toolbar
title:

```kotlin
override fun onCreate(savedInstanceState: Bundle?) { 
super.onCreate(savedInstanceState) 
setContentView(R.layout.activity_main) initToolbar()
    forecastList.layoutManager = LinearLayoutManager(this)
    attachToScroll(forecastList)
    async {
        val result = RequestForecastCommand(94043).execute()
        uiThread {
            val adapter = ForecastListAdapter(result) {
            startActivity<DetailActivity>(DetailActivity.ID to it.id,
                        DetailActivity.CITY_NAME to result.city)
            }
            forecastList.adapter = adapter
            toolbarTitle = "${result.city} (${result.country})"
        } 
    }
}
```

`DetailActivity` also needs some layout modifications:
```kotlin
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">

    <include layout="@layout/toolbar"/>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        android:gravity="center_vertical"
        android:paddingTop="@dimen/activity_vertical_margin"
        android:paddingLeft="@dimen/activity_horizontal_margin"
        android:paddingRight="@dimen/activity_horizontal_margin"
        tools:ignore="UseCompoundDrawables">
       ....
    </LinearLayout>

</LinearLayout>
```

The `toolbar` property is specified the same way. `DetailActivity` will initialise the toolbar too, set the title and enable the up navigation icon:
```kotlin
override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail)

    initToolbar()
    toolbarTitle = intent.getStringExtra(CITY_NAME) 
    enableHomeAsUp { onBackPressed() }
    ...
}
```

Interfaces can help us extract common code from classes that share similar behaviours. It can be used as an alternative way of composition that will keep our code better organised, and it will be easier to reuse. Think where interfaces can help you write better code.

[DrawerArrowDrawable²⁷]:https://developer.android.com/reference/android/support/v7/graphics/drawable/DrawerArrowDrawable.html