# Simplifying setOnClickListener()
I will explain how this works using a typical example in Android: the `View.setOnClickListener()` method. If we want to implement a click listener behaviour in Java, we first need to write the `OnClickListener` interface:

```kotlin
public interface OnClickListener {
    void onClick(View v);
}
```

And then we write an anonymous class that implements this interface:

```kotlin
view.setOnClickListener(new OnClickListener(){
    @Override
    public void onClick(View v) {
        Toast.makeText(v.getContext(), "Click", Toast.LENGTH_SHORT).show();
    }
})
```

This would be the transformation of the code into Kotlin (using Anko `toast` function):

```kotlin
view.setOnClickListener(object : OnClickListener {
    override fun onClick(v: View) {
        toast("Click")
    }
}
```

Luckily, Kotlin allows some optimisations over Java libraries, and any function that receives an interface with a single function can be substituted by the function. It will work as if we had defined `setOnclickListener()` like this:

```kotlin
fun setOnClickListener(listener: (View) -> Unit)
```

A lambda expression is defined by the parameters of the function to the left of the arrow (surrounded by parentheses), and the return value to the right. In this case, we get a `View` and return `Unit` (nothing). So with this in mind, we can simplify the previous code a little:

```kotlin
view.setOnClickListener({ view -> toast("Click")})
```

Nice difference! While defining a function, we must use brackets and specify the parameter values to the left of the arrow and the code the function will execute to the right. We can even get rid of the left part if the parameters are not being used:

```kotlin
view.setOnClickListener({ toast("Click") })
```

If the function is the last one in the parameters of the function, we can move it out of the parentheses:

```kotlin
view.setOnClickListener() { toast("Click") }
```

And, finally, if the function is the only parameter, we can get rid of the parentheses:

```kotlin
view.setOnClickListener { toast("Click") }
```

More than five times smaller than the original code in Java, and much easier to understand what is doing. Really impressive.