# Constructors

By default, all constructors are `public`, which means they can be used from any scope where their class is visible. But we can make a constructor private using this specific syntax:
```kotlin
class C private constructor(a: Int) { ... }
```