# Refactoring our code

Now it’s time to change our code so that we can start making use of Kotlin Android Extensions. The modifications are fairly simple.

Let’s start with MainActivity. We are currently only using a `forecastList` view, which is in fact a `RecyclerView`. But we can clean this code a little bit. First, add the synthetic import for the `activity_main` XML:

```kotlin
import kotlinx.android.synthetic.main.activity_main.*
```

As said before, we use the id to access the views, so I’m changing the id of the `RecyclerView` so thatit doesn’t use underscores, but a more appropriate name for a Kotlin variable. The XML results into this:
```kotlin
<FrameLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">
    <android.support.v7.widget.RecyclerView
        android:id="@+id/forecastList"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</FrameLayout>
```

And now we can just get rid of the `find` line:
```kotlin
override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    forecastList.layoutManager = LinearLayoutManager(this)
    ...
}
```

The simplification was minimal because this layout is very simple. But the `ForecastListAdapter` can also benefit from the use of this plugin. Here, we can use the mechanism to bind the properties into a view, which will help us remove all the find code inside the `ViewHolder`. 

First, add the synthetic import for `item_forecast`:

```kotlin
import kotlinx.android.synthetic.main.item_forecast.view.*
```

And now we can use the properties in `itemView` property inside the ViewHolder. In fact you can use those properties over any view, but it will obviously crash if the view doesn’t contain the requested sub-views.

We don’t need to declare properties for the views anymore, we now can just use them:

```kotlin
class ViewHolder(view: View, val itemClick: (Forecast) -> Unit) :
        RecyclerView.ViewHolder(view) {
    fun bindForecast(forecast: Forecast) {
        with(forecast){
            Picasso.with(itemView.ctx).load(iconUrl).into(itemView.icon)
            itemView.date.text = date
            itemView.description.text = description
            itemView.maxTemperature.text = "${high}"
            itemView.minTemperature.text = "${low}"
            itemView.onClick { itemClick(this) }
        } 
    }
}
```

Kotlin Android Extensions plugin helps us reduce some more boilerplate and minimise the code required to access our views. Check the latest changes at the repository.

