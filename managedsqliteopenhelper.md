# ManagedSqliteOpenHelper

Anko provides a powerful `SqliteOpenHelper` which simplifies things a lot. When we use a regular SqliteOpenHelper, we need to call `getReadableDatabase()` or `getWritableDatabase()`, and then we can perform our queries over the object we get. After that, we shouldn’t forget to call `close()`. With a `ManagedSqliteOpenHelper` we just do:

```kotlin
forecastDbHelper.use {
    ...
}
```

Inside the lambda we can use `SqliteDatabase` functions directly. How does it work? It’s really interesting to read the implementation of Anko functions, you can learn a lot of Kotlin from it:
```kotlin
public fun <T> use(f: SQLiteDatabase.() -> T): T {
    try {
        return openDatabase().f()
    } finally {
        closeDatabase()
    }
}
```

First, `use` receives a function that will be used as an extension function by `SQLiteDatabase`. This means we can use `this` inside the brackets, and we’ll be referring to the `SQLiteDatabase` object. This extension function can return a value, so we could do something like this:

```kotlin
val result = forecastDbHelper.use {
    val queriedObject = ...
    queriedObject
}
```

Take in mind that, inside a function, the last line represents the returned value. As T doesn’t have any restrictions, we can return any value. Even `Unit` if we don’t want to return anything.

By using a `try-finally`, the `use` function makes sure that the database is closed no matter the extended function succeeds or crashes.

Besides, we have a lot of other really useful extension functions over `SqliteDatabase` that we’ll be using later. But for now let’s define our tables and implement the SqliteOpenHelper.