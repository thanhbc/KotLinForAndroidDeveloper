# Ranges

It’s difficult to explain flow control without talking about ranges. But their scope is much wider. Range expressions make use of an operator in the form of “..” that is defined implementing a `RangeTo` function.

Ranges help simplify our code in many creative ways. For instance we can convert this:
```kotlin
if(i >= 0 && i <= 10) 
    println(i)
```

Into this:
```kotlin
if (i in 0..10)
    println(i)
```

`Range` is defined by any type that can be compared, but for numerical types the compiler will optimise it by converting it to simpler analogue code in Java, to avoid the extra overhead. The numerical ranges can also be iterated, and the loops are optimised too by converting them to the same bytecode a `for` with indices would use in Java:
```kotlin
for (i in 0..10)
    println(i)
```

Ranges are incremental by default, so something like:
```kotlin
for (i in 10..0)
    println(i)
```

Would do nothing. You can, however, use the function `downTo`:
```kotlin
for(i in 10 downTo 0)
    println(i)
```

We can define a spacing different from 1 among the values in a range by using step:
```kotlin
for (i in 1..4 step 2) println(i)

for (i in 4 downTo 1 step 2) println(i)
```

If you want to create an open range (which excludes the last item), you can use the function `until`:
```kotlin
for (i in 0 until 4) println(i)
```

This previous line will print from 0 to 3, but will skip the last value. This means that `0 until 4 == 0..3`. For iterations over lists it could be easier to understand if we use `for (i in 0 until list.size)` instead of `for (i in 0..list.size - 1)`.

As mentioned before, there are really creative ways to use ranges. For instance, an easy way to get the list of Views inside a `ViewGroup` would be:

```kotlin
val views = (0 until viewGroup.childCount).map { viewGroup.getChildAt(it) }
```

The mix of ranges and functional operators prevents from having to use an explicit loop to iterate over the collection, and the creation of an explicit list where we add the views. Everything is done in a single line.

If you want to know more about how ranges are implemented and a lot more examples and useful information, you can go to [Kotlin reference²⁵].

[Kotlin reference²⁵]:https://kotlinlang.org/docs/reference/ranges.html