# Introduction

Nếu bạn thấy rằng Java 7 đã lỗi thời và bạn cần một ngôn ngữ hiện đại hơn. Xin chúc mừng! Như bạn đã biết, mặc dù với Java 8, bao gồm rất nhiều cải tiến mà chúng ta mong chờ ở một ngôn ngữ hiện đại, chúng ta, các nhà phát triển Android vẫn bắt buộc phải sử dụng Java 7. Đây là một phần của vấn đề pháp lý. Nhưng thậm chí không có sự giới hạn này, nếu các thiết bị Android hôm nay bắt đầu sử dụng máy ảo để có thể chạy Java 8, chúng ta cũng không thể bắt đầu sử dụng chúng cho đến khi các thiết bị Android hiện tại lỗi thời và không ai dùng chúng. Vì vậy tôi e là chúng ta sẽ không thấy thời điểm này sớm.

Nhưng không phải tất cả điều mất. Cám ơn vì đã sử dụng máy ảo Java (JVM), chúng ta có thể viết các ứng dụng Android sử dụng bất kỳ ngôn ngữ nào có thể biên dịch ra bytecode, để JVM có thể hiểu được.

Bạn có thể tưởng tượng, có rất nhiều lựa chọn như là Groovy, Scala, Clojure và tất nhiên là cả Kotlin. Thực tế, chỉ có một số trong chúng có thể được xem là giải pháp thay thế thực sự.

Bất kỳ ngôn ngữ nào cũng có ưu và nhược điểm, và tôi đề nghị bạn nên xem qua một số trong đó nếu như bạn không chắc chắn ngôn ngữ nào sẽ được sử dụng.