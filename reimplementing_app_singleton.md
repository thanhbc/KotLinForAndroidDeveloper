# Reimplementing App Singleton

Delegates can help us in this situation. We know that our singleton is not going to be null, but we can’t use the constructor to assign the property. So we can make use of a `lateinit` delegate:

```kotlin
class App : Application() {
    companion object {
        var instance: App by Delegates.notNull()
    }

    override fun onCreate() {
            super.onCreate()
            instance = this
    }
}
```

The problem with this solution is that we could change the value of this instance from anywhere in the App, because a `var` property is required if we want to use `lateinit`. That’s easy to solve by using a private `set`:
```kotlin
companion object {
    lateinit var instance: App
        private set
}
```

But we’ll be making use of our custom delegate instead:
```kotlin
companion object {
    var instance: App by DelegatesExt.notNullSingleValue()
}
```

Though, in this case, `lateinit` is probably the most simple option, I wanted to show you how to create a custom property delegate and use it in your code.