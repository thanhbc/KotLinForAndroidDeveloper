# Shaping the domain layer

Now we’ll create a new package representing the `domain` layer. It will contain some `Commands` in charge of performing the use cases of the app.

First, a definition of a `Command` is required:
```kotlin
public interface Command<T> {
    fun execute(): T
}
```

These commands will execute an operation and return an object of the class specified in its generic type. It’s interesting to know that **every function in Kotlin returns a value**. By default, if nothing is specified, it will return an object of the `Unit` class. So if we want our `Command` to return nothing, we can specify `Unit` as its type.

Interfaces in Kotlin are more powerful than Java (prior to Java 8), because they can contain code. But for now, we don’t need more than what we could do in a Java interface. Future chapters will elaborate this topic further.

The first command needs to request the forecast to the API and convert it to domain classes. This is the definition of the domain classes:

```kotlin
data class ForecastList(val city: String, val country: String,
                        val dailyForecast:List<Forecast>)

data class Forecast(val date: String, val description: String, val high: Int,
                    val low: Int)
                    
                   ```
These classes will probably need to be reviewed in the future, when more features are added. But the data they keep is enough for now.                   

Classes must be mapped from the data to the domain model, so the next task will be to create a `DataMapper`:

```kotlin
public class ForecastDataMapper {
    fun convertFromDataModel(forecast: ForecastResult): ForecastList {
        return ForecastList(forecast.city.name, forecast.city.country,
                convertForecastListToDomain(forecast.list))
    private fun convertForecastListToDomain(list: List<Forecast>):
            List<ModelForecast> {
        return list.map { convertForecastItemToDomain(it) }
    }
    private fun convertForecastItemToDomain(forecast: Forecast): ModelForecast {
        return ModelForecast(convertDate(forecast.dt),
                forecast.weather[0].description, forecast.temp.max.toInt(),
                forecast.temp.min.toInt())
}
    private fun convertDate(date: Long): String {
        val df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault())
        return df.format(date * 1000)
    }
}
```
As we are using two classes with the same name, we can give a specific name to one of them so that we don’t need to write the complete package:

```kotlin
import com.antonioleiva.weatherapp.domain.model.Forecast as ModelForecast
```

Another interesting thing about this code is the way to convert the forecast list from the data to the domain model:
```kotlin
return list.map { convertForecastItemToDomain(it) }
```

In a single line, we can loop over the collection and return a new list with the converted items. Kotlin provides a good set of functional operations over lists, which apply an operation for all the items in a list and transform them in any way. This is one of the most powerful features in Kotlin for developers used to Java 7. We’ll take a look at all the different transformations very soon. It’s important to know they exist, because it will be much easier to find places where these functions can save a lot of time and boilerplate.

And now, everything is ready to write the command:

```kotlin
class RequestForecastCommand(val zipCode: String) :
                Command<ForecastList> {
    override fun execute(): ForecastList {
        val forecastRequest = ForecastRequest(zipCode)
        return ForecastDataMapper().convertFromDataModel(
                forecastRequest.execute())
    }
}```