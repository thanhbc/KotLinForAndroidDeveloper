# Extending the language

Thanks to these transformations, we can create our own builders and code blocks. We’ve already been using some interesting functions such as with. A simpler implementation would be:

```kotlin
inline fun <T> with(t: T, body: T.() -> Unit) { t.body() }
```

This function gets an object of type T and a function that will be used as an extension function. The implementation just takes the object and lets it execute the function. As the second parameter of the function is another function, it can be brought out of the parentheses, so we can create a block of code where we can use `this` and the public properties and functions of the object directly:

```kotlin
with(forecast) {
    Picasso.with(itemView.ctx).load(iconUrl).into(iconView)
    dateView.text = date
    descriptionView.text = description
    maxTemperatureView.text = "$high"
    minTemperatureView.text = "$low"
    itemView.setOnClickListener { itemClick(this) }
}
```

>Inline functions
>>Inline functions are a bit different from regular functions. An inline function will be substituted by its code during compilation, instead of really calling to a function. It will reduce memory allocations and runtime overhead in some situations. For instance, if we have a function as an argument, a regular function will internally create an object that contains that function. On the other hand, inline functions will substitute the code of the function in the place where it is called, so it won’t require an internal object for that.

Another example: we could create blocks of code that are only executed if the version is Lollipop or newer:
```kotlin
inline fun supportsLollipop(code: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        code()
    }
}
```

It just checks the version and executes the code if it meets the requirements. Now we could do:
```kotlin
supportsLollipop {
    window.setStatusBarColor(Color.BLACK)
}
```

For instance, Anko is also based on this idea to implement the DSL for Android layouts. You can also check an example from Kotlin reference, where a DSL to write [HTML²⁰] from code is created.

[HTML²⁰]:http://kotlinlang.org/docs/reference/type-safe-builders.html