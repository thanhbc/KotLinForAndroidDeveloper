# Collections and functional operations
We’ve been using collections before in this project, but now it’s time to show how powerful they are in combination with functional operations. The good part about functional programming is that instead of explaining how we do things, we just say what we want to do. For instance, if we want to filter a list, instead of creating a list, iterating over the original one and add the items to the new if they satisfy a condition, we just use a filter function and specify which filter we want to use. That way, we can say a lot more using less code.

Although we can just use Java collections, Kotlin provides a good set of native interfaces you will want to use:

- **Iterable**: The parent class. Any classes that inherit from this interface represent a sequence of elements we can iterate over.
- **MutableIterable**: Iterables that support removing items during iteration.
- **Collection**: This class represents a generic collection of elements. We get access to functions that return the size of the collection, whether the collection is empty, contains an item or a set of items. All the methods for this kind of collections are only to request data, because collections are immutable.
- **MutableCollection**: `a Collection` that supports adding and removing elements. It provides extra functions such as add, `remove` or `clear` among others.
- **List**: Probably the most popular collection type. It represents a generic ordered collection of elements. As it’s ordered, we can request an item by its position, using the `get` function.
- **MutableList**: a List that supports adding and removing elements.
- **Set**: an unordered collection of elements that doesn’t support duplicate elements.
- **MutableSet**: a Set that supports adding and removing elements.
- **Map**: a collection of key-value pairs. The keys in a map are unique, which means we cannot have two pairs with the same key in a map.
- **MutableMap**: a Map that supports adding and removing elements.

This is the set of functional operations we have available over the different collections. I want to show you a little definition and an example. It is useful to know what the options are, because that way it’s easier to identify where these functions can be used.