# Testing your App

We are reaching the end of this trip. You already learned most Kotlin features throughout this book, but you are probably wondering if you can test your Android Apps using Kotlin exclusively. The answer is: of course!

In Android we have a couple of well differentiated tests: unit tests and instrumentation tests. This book is obviously not meant to teach you how tests should be done, there are whole books dedicated to that matter. My goal in this chapter is to explain how to prepare your environment to be able to write some tests, and show you that Kotlin also works fine for testing.