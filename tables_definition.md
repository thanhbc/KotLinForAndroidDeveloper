# Tables definition

The creation of a couple of objects that represent our tables will be helpful to avoid misspelling table or column names and repetition. We need two tables: one will save the info of the city and another one the forecast of a day. This second table will have a relationship field to the first one.

`CityForecastTable` first provides the name of the table and then the set of columns it needs: an `id` (which will be the zip code of the city), the name of the city and the country.
```kotlin
object CityForecastTable {
    val NAME = "CityForecast"
    val ID = "_id"
    val CITY = "city"
    val COUNTRY = "country"
}
```

`DayForecast` has some more info, so it will need the set of columns you can see below. The last column, `cityId`, will keep the id of the `CityForecast` this forecast belongs to.

```kotlin
object DayForecastTable {
    val NAME = "DayForecast"
    val ID = "_id"
    val DATE = "date"
    val DESCRIPTION = "description"
    val HIGH = "high"
    val LOW = "low"
    val ICON_URL = "iconUrl"
    val CITY_ID = "cityId"
}
```