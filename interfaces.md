# Interfaces

Interfaces in Kotlin are more powerful than in Java 7. If you’ve worked with Java 8, similarities are much closer there. In Kotlin, we can use interfaces the way we are used in Java. Imagine we hav some animals, and some of them can fly. This is the interface we could have for flying animals:
```kotlin
interface FlyingAnimal {
    fun fly()
}
```
Both birds and bats can fly by moving their wings. So let’s create a couple of classes for them:
```kotlin
class Bird : FlyingAnimal {
    val wings: Wings = Wings()
    override fun fly() = wings.move()
}

class Bat : FlyingAnimal {
    val wings: Wings = Wings()
    override fun fly() = wings.move()
}
```

When a couple of classes extend from an interface, it’s very typical they both share the same implementation. However, Java 7 interfaces can only define the behaviour, but not implement it.

Kotlin interfaces, on the other hand, are able to implement functions. The only difference from a class is that they are stateless, so the properties that need a backing field will need to be overridden by the class. The class will be in charge of saving the state of interface properties.

We can make the interface implement the `fly` function:
```kotlin
interface FlyingAnimal {
    val wings: Wings
    fun fly() = wings.move()
}
```

As mentioned, classes need to override the property:
```kotlin
class Bird : FlyingAnimal {
    override val wings: Wings = Wings()
}

class Bat : FlyingAnimal {
    override val wings: Wings = Wings()
}
```
And now both birds and bats can fly:
```kotlin
val bird = Bird()
val bat = Bat()

bird.fly()
bat.fly()
```