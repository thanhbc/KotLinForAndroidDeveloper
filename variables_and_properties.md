# Variables and properties

Trong Kotlin, tất cả mọi thứ đều là object. Chúng tôi không tìm thấy kiểu dữ liệu primitive như trong Java. Điều đó rất hữu ích, vì chúng ta có một cách để làm việc với tất cả các loại kiểu dữ liệu.