# Test that everything works
Chúng ta sẽ thêm một số code để test Kotlin Android Extensions đang hoạt động. Mình vẫn chưa giải thích nhiều về nó, nhưng mình muốn chắc chắn rằng nó hoạt động. Nó có lẽ là phần khó nhất trong phần cấu hình này.

Đầu tiên, đi đến `activity_main.xml` và set một id cho TextView:
```xml
<TextView
    android:id="@+id/message"
    android:text="@string/hello_world"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"/>
```

Bây giờ, import synthetic vào activity (đừng xoắn nếu bạn vẫn chưa hiểu về nó):
```kotlin
import kotlinx.android.synthetic.main.activity_main.*
```

Ở `onCreate`, bây giờ bạn có thể truy xuất TextView đó một cách trực tiếp:

```kotlin
override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    message.text = "Hello Kotlin!"
}
```

Phải cám ơn Kotlin về khả năng tương thích với Jva, chúng ta có thể sử dụng phương thức setters và getters từ Java như một property (thuộc tính) trong Kotlin. Chúng ta sẽ nói về các properties sau, nhưng lưu ý rằng chúng ta có thể sử dụng `message.text` thay vì `message.setText`. Trình biên dịch sẽ sử dụng các phương thức thực sự của Java, vì vậy sẽ không có vấn đề gì về hiệu suất khi sử dụng nó.

Bây giờ chay thử app và mọi thứ điều hoạt động tốt. Kiểm tra message `TextView` hiển thị nội dung mới. Nếu bạn có bất kỳ nghi ngờ gì hoặc muốn xem lại code, có thể xem tại [Kotlin for Android Developers repository¹¹](https://github.com/antoniolg/Kotlin-for-Android-Developers). Tôi sẽ commit từng chap, khi chap đó có thay đổi gì trong code, hãy chắn chắn rằng bạn review và kiểm tra những thay đổi đó.

Các chương tiếp theo sẽ cover một vài thứ mới mà bạn đang xem trong file `MainActivity` đã được convert. Một khi bạn hiểu được sự thay đổi giữa Java và Kotlin, bạn sẽ có thể tự viết code mới dễ dàng hơn.